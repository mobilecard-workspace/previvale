/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.test;

import com.addcel.utils.AddcelCrypto;

public class TestEncrypt {

	public static void main(String[] args) {
		System.out.println("Empezando test ...");
		
		String numero = "5064500126446302";
		String vigencia = "08/23";
		String cvv = "868";
		
		System.out.println("Numero enc: " + AddcelCrypto.encryptTarjeta(numero));
		System.out.println("Vigencia enc: " + AddcelCrypto.encryptTarjeta(vigencia));
		System.out.println("CVV enc: " + AddcelCrypto.encryptTarjeta(cvv));
		
		System.out.println("Numero dec: " + AddcelCrypto.decryptTarjeta(AddcelCrypto.encryptTarjeta(numero)));
		System.out.println("Vigencia dec: " + AddcelCrypto.decryptTarjeta(AddcelCrypto.encryptTarjeta(vigencia)));
		System.out.println("CVV dec: " + AddcelCrypto.decryptTarjeta(AddcelCrypto.encryptTarjeta(cvv)));
	}
	
}
