/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.response;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EstatusTarjetaNegocioResponse {

	private String pan; // TDC NUMBER
	private String vigencia;
	private String ct;
	private String nombre;
	private Boolean determinada;
	private Boolean isMobilecard;
	private String codigo;
	private Double balance;
	private String date;
	private List<String> movements;
	private String domAmex;
	private String cpAmex;
	private Integer tipoTarjeta;
	private String phoneNumberActivation;
	private Date fechaRegistro;
	private String representanteLegal;

}
