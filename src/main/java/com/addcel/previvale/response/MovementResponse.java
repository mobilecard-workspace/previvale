/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovementResponse {

	private String cantidad;
	private String establecimiento;
	private String fecha;
	private String status;
	
}
