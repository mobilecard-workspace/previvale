/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseResponse {

	private String authorization;
	private int previvaleCode;
	private long idPeticion;
	private Long idPrevivaleCardStock;
	
}
