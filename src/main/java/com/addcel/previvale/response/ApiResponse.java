/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiResponse {

	private int code;
	private String message;
	
	//@JsonInclude(value = Include.NON_EMPTY)
    private Object data;
	
}
