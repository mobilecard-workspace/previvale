/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.services;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.previvale.constants.MobileCardConstants;
import com.addcel.previvale.constants.PreviValeConstants;
import com.addcel.previvale.constants.StatusConstants;
import com.addcel.previvale.response.ApiResponse;
import com.addcel.previvale.response.BaseResponse;
import com.addcel.previvale.response.MovementResponse;
import com.addcel.previvale.util.DataValidator;
import com.addcel.previvale.util.PersistRequest;
import com.addcel.previvale.util.ValidarUsuarios;
import com.addcel.previvale.ws.SOAPConnector;
import com.addcel.utils.AddcelCrypto;

import previvale.wsdl.ClassGetCreditLimit;
import previvale.wsdl.ClassGetMovementsMonth;
import previvale.wsdl.GetCreditLimitAndAvailableData;
import previvale.wsdl.GetCreditLimitAndAvailableDataResponse;
import previvale.wsdl.GetMovementsMonth;
import previvale.wsdl.GetMovementsMonthResponse;

@Service
public class MovimientosTarjetaService {
	
	@Autowired
	private DataValidator dataVal;
	
	@Autowired
	private PersistRequest persistReq;
	
	@Autowired
	private SOAPConnector soapConnect;
	
	@Autowired
	private ValidarUsuarios validarUsuarios;
	
	private static final Logger LOGGER = LogManager.getLogger(MovimientosTarjetaService.class);
	
	public ApiResponse movimientosTarjeta(Long idUsuario, Long idEstablecimiento, String tarjeta, String fechaIni, String fechaFin) {
		int statusCode = StatusConstants.UNSUCCESS_WS_PREVIVALE;
		String statusMsg = StatusConstants.UNSUCCESS_WS_PREVIVALE_MSG;
		List<MovementResponse> movRespList = null;
		
		Map<String, Object> map = validarUsuarios.getUserStatus(idUsuario, idEstablecimiento);
		
		if((Integer) map.get("status") == MobileCardConstants.USUARIO_ACTIVO || 
				(Integer) map.get("status") == MobileCardConstants.USUARIO_ACTIVO_2) {
			LOGGER.debug("Usuario encontrado y activo!");
			
			LOGGER.debug("Realizando la busqueda de movimientos ...");
			LOGGER.debug("Fecha Inicial: " + fechaIni);
			LOGGER.debug("Fecha Final: " + fechaFin);
			
			if(!dataVal.validateFecha(fechaIni) || !dataVal.validateFecha(fechaFin)) {
				LOGGER.error("El formato de las fechas es incorrecto");
				return new ApiResponse(StatusConstants.BAD_DATE, StatusConstants.BAD_DATE_MSG, null);
			}
			
			tarjeta = AddcelCrypto.decryptHard(tarjeta);
			
			GetMovementsMonth getMovs = new GetMovementsMonth();
			getMovs.setDate(Integer.parseInt(fechaIni));
			getMovs.setEndDate(Integer.parseInt(fechaFin));
			getMovs.setCard(tarjeta);
			
			GetMovementsMonthResponse getMovsResp = null;
			
			try {
				getMovsResp = (GetMovementsMonthResponse) soapConnect.callWebService(getMovs, PreviValeConstants.SOAP_ACTION_GETMOVEMENTS);
			} catch(Exception ex) {
				LOGGER.error("Error al consumir el WS de PreviVale: " + ex.getMessage());
				ex.printStackTrace();
				return new ApiResponse(StatusConstants.ERROR_WS_PREVIVALE, StatusConstants.ERROR_WS_PREVIVALE_MSG, movRespList);
			}
			
			List<ClassGetMovementsMonth> movsList = getMovsResp.getGetMovementsMonthResult().getClassGetMovementsMonth();
			
			LOGGER.debug("Cantidad de movimientos: " + movsList.size());
			
			if(movsList.get(0).getAmount().contains("Error") || movsList.get(0).getDate().contains("Error")) {
				statusCode = StatusConstants.MOVEMENTS_ERROR;
				statusMsg = StatusConstants.MOVEMENTS_ERROR_MSG;
				
				LOGGER.warn("El servicio de consulta de movimientos regreso un error: " + movsList.get(0).getAmount() 
						+ " - " + movsList.get(0).getDate());
			} else {
				statusCode = StatusConstants.SUCCESS_OPERATION;
				statusMsg = StatusConstants.MOVS_CARD_MSG;
				
				LOGGER.info("Se obtuvieron correctamente los movimientos:");
								
				movRespList = new LinkedList<MovementResponse>();
				
				for(ClassGetMovementsMonth mov : movsList) {
					MovementResponse movResp = new MovementResponse();
					movResp.setCantidad(mov.getAmount());
					movResp.setEstablecimiento(mov.getCommerce());
					movResp.setFecha(mov.getDate());
					movResp.setStatus(mov.getStatus());
					LOGGER.debug("Movimiento: " + movResp.toString());
					
					if(!mov.getDate().isEmpty()) movRespList.add(movResp);
				}
			}
			
			persistReq.saveRequest(PreviValeConstants.SOAP_ACTION_GETMOVEMENTS, getMovs, Arrays.toString(movsList.toArray()), idUsuario, idEstablecimiento);
		} else {
			LOGGER.error("El usuario esta bloqueado, no se podra obtener los movimientos de la tarjeta");
			return new ApiResponse(StatusConstants.USER_BLOCKED, StatusConstants.USER_BLOCKED_MSG, movRespList);
		}
		
		return new ApiResponse(statusCode, statusMsg, movRespList);
	}
	
	public ApiResponse creditoTarjeta(Long idUsuario, Long idEstablecimiento, String tarjeta) {
		BaseResponse baseResp = null;
		int statusCode = StatusConstants.UNSUCCESS_WS_PREVIVALE;
		String statusMsg = StatusConstants.UNSUCCESS_WS_PREVIVALE_MSG;
		
		Map<String, Object> map = validarUsuarios.getUserStatus(idUsuario, idEstablecimiento);
		
		if((Integer) map.get("status") == MobileCardConstants.USUARIO_ACTIVO || 
				(Integer) map.get("status") == MobileCardConstants.USUARIO_ACTIVO_2) {
			LOGGER.debug("Usuario encontrado y activo!");
			
			tarjeta = AddcelCrypto.decryptTarjeta(tarjeta);
			
			ClassGetCreditLimit classGetCredit = new ClassGetCreditLimit();
			classGetCredit.setCard(tarjeta);
			GetCreditLimitAndAvailableData getCredit = new GetCreditLimitAndAvailableData();
			getCredit.setGetCreditLimit(classGetCredit);
			
			GetCreditLimitAndAvailableDataResponse getCreditResp = null;
			
			try {
				getCreditResp = (GetCreditLimitAndAvailableDataResponse) soapConnect.callWebService(getCredit, 
						PreviValeConstants.SOAP_ACTION_GETCREDITLIMIT);
			} catch(Exception ex) {
				LOGGER.error("Error al consumir el WS de PreviVale: " + ex.getMessage());
				ex.printStackTrace();
				return new ApiResponse(StatusConstants.ERROR_WS_PREVIVALE, StatusConstants.ERROR_WS_PREVIVALE_MSG, baseResp);
			}
			
			LOGGER.debug("Auth Resp: " + getCreditResp.getGetCreditLimitAndAvailableDataResult().getAuthorization());
			LOGGER.debug("Code Resp: " + getCreditResp.getGetCreditLimitAndAvailableDataResult().getCodeReturn());
			
			baseResp = new BaseResponse();
			baseResp.setAuthorization(getCreditResp.getGetCreditLimitAndAvailableDataResult().getAuthorization());
			baseResp.setPrevivaleCode(getCreditResp.getGetCreditLimitAndAvailableDataResult().getCodeReturn());
			
			if(getCreditResp.getGetCreditLimitAndAvailableDataResult().getCodeReturn() == 0) {
				statusCode = StatusConstants.SUCCESS_OPERATION;
				statusMsg = StatusConstants.CREDIT_CARD_MSG;
			}
			
			long idPeticion = persistReq.saveRequest(PreviValeConstants.SOAP_ACTION_GETCREDITLIMIT, classGetCredit, baseResp.toString(), idUsuario, idEstablecimiento);
			baseResp.setIdPeticion(idPeticion);
		} else {
			LOGGER.error("El usuario esta bloqueado, no se podra obtener el credito disponible de la tarjeta");
			return new ApiResponse(StatusConstants.USER_BLOCKED, StatusConstants.USER_BLOCKED_MSG, baseResp);
		}
		
		return new ApiResponse(statusCode, statusMsg, baseResp);
	}

}
