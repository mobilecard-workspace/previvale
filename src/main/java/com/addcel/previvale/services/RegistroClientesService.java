/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.services;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.previvale.constants.MobileCardConstants;
import com.addcel.previvale.constants.PreviValeConstants;
import com.addcel.previvale.constants.StatusConstants;
import com.addcel.previvale.domain.Estados;
import com.addcel.previvale.domain.PreviValeCardStock;
import com.addcel.previvale.domain.StpCuentasClabe;
import com.addcel.previvale.domain.TUsuarios;
import com.addcel.previvale.domain.TarjetasUsuario;
import com.addcel.previvale.repositories.EstadosRepository;
import com.addcel.previvale.repositories.PreviValeCardStockRepository;
import com.addcel.previvale.repositories.StpCuentasClabeRepository;
import com.addcel.previvale.repositories.TUsuariosRepository;
import com.addcel.previvale.repositories.TarjetasUsuarioRepository;
import com.addcel.previvale.response.ApiResponse;
import com.addcel.previvale.response.BaseResponse;
import com.addcel.previvale.util.Foliador;
import com.addcel.previvale.util.PersistRequest;
import com.addcel.previvale.ws.SOAPConnector;
import com.addcel.utils.AddcelCrypto;

import previvale.wsdl.ClassUserRegister;
import previvale.wsdl.UserRegister;
import previvale.wsdl.UserRegisterResponse;

@Service
public class RegistroClientesService {
	
	@Autowired
	private TUsuariosRepository tUsuariosRepo;
	
	@Autowired
	private EstadosRepository estadosRepo;
	
	@Autowired
	private TarjetasUsuarioRepository tarjetasUsuarioRepo;
	
	@Autowired
	private PreviValeCardStockRepository cardStockRepo;
	
	@Autowired
	private StpCuentasClabeRepository stpCuentasClabeRepo;
	
	@Autowired
	private PersistRequest persistReq;
				
	@Autowired
	private SOAPConnector soapConnect;
	
	@Autowired
	private Foliador foliador;
	
	private static final Logger LOGGER = LogManager.getLogger(RegistroClientesService.class);
	
	public ApiResponse registroCliente(long idUsuario, int idApp) {
		BaseResponse baseResp = null;
		int statusCode = StatusConstants.UNSUCCESS_WS_PREVIVALE;
		String statusMsg = StatusConstants.UNSUCCESS_WS_PREVIVALE_MSG;
		
		LOGGER.debug("Buscando en la BD al usuario: " + idUsuario);
		TUsuarios usuario = tUsuariosRepo.findOne(idUsuario);
		
		if(usuario != null && (usuario.getIdUsrStatus() == MobileCardConstants.USUARIO_ACTIVO || 
				usuario.getIdUsrStatus() == MobileCardConstants.USUARIO_ACTIVO_2)) {
			LOGGER.debug("Usuario encontrado: " + usuario.toString());
			LOGGER.info("Buscando una tarjeta disponible en el sotck ...");
			
			List<PreviValeCardStock> cardStockList = 
					cardStockRepo.findFirst1ByActivaAndIdUsuarioAndIdEstablecimiento(MobileCardConstants.PREVIVALE_CARD_STATUS_INACTIVE, null, null);
			
			if(cardStockList != null && cardStockList.size() > 0) {
				PreviValeCardStock cardStock = cardStockList.get(0);
				String tarjeta = AddcelCrypto.decryptTarjeta(cardStock.getNumero());
				LOGGER.info("Tarjeta econtrada para asignar al usuario: " + cardStock.getUltimosNumeros());
				
				LOGGER.debug("Buscando una cuenta CLABE para asignar a esa tarjeta ...");
				StpCuentasClabe stpCuentasClabe = stpCuentasClabeRepo.findClabeActiva(MobileCardConstants.CLABE_ACTIVA);
				
				if(stpCuentasClabe != null) {
					LOGGER.debug("Se ha encontrado la siguiente cuenta CLABE para asignar: " + stpCuentasClabe.toString());
				} else {
					LOGGER.warn("No se ha encontrado ninguna cuenta CLABE activa para asignar a la tarjeta de PreviVale,"
							+ "verificar que haya CLABEs activas y suficientes en la tabla de stp_cuentas_clabe");
					return new ApiResponse(StatusConstants.CLABE_NOTFOUND, StatusConstants.CLABE_NOTFOUND_MSG, baseResp);
				}
				
				ClassUserRegister classUserReg = new ClassUserRegister();
				classUserReg.setCard(tarjeta);
				classUserReg.setName(usuario.getUsrNombre());
				classUserReg.setLastName(usuario.getUsrApellido());
				classUserReg.setMothersLastName(usuario.getUsrMaterno());
				classUserReg.setCurp(usuario.getUsrCurp());
				classUserReg.setRfc(usuario.getUsrRfc());
				classUserReg.setColony(usuario.getUsrColonia());
				classUserReg.setCp(usuario.getUsrCp());
				classUserReg.setMunicipality(usuario.getUsrCiudad());
				classUserReg.setStreet(usuario.getUsrDireccion());
				classUserReg.setState(obtenerEstado(estadosRepo.findByIdedo(usuario.getUsrIdEstado()).getNombre()));
				classUserReg.setFolio(foliador.generarFolio());
				
				UserRegister userReg = new UserRegister();
				userReg.setUserRegister(classUserReg);
				
				UserRegisterResponse userRegResp = null;
				
				try {
					userRegResp = (UserRegisterResponse) soapConnect.callWebService(userReg, PreviValeConstants.SOAP_ACTION_USERREGISTER);
				} catch(Exception ex) {
					LOGGER.error("Error al consumir el WS de PreviVale: " + ex.getMessage());
					ex.printStackTrace();
					return new ApiResponse(StatusConstants.ERROR_WS_PREVIVALE, StatusConstants.ERROR_WS_PREVIVALE_MSG, baseResp);
				}
				
				LOGGER.debug("Auth Resp: " + userRegResp.getUserRegisterResult().getAuthorization());
				LOGGER.debug("Code Resp: " + userRegResp.getUserRegisterResult().getCodeReturn());
				
				baseResp = new BaseResponse();
				baseResp.setAuthorization(userRegResp.getUserRegisterResult().getAuthorization());
				baseResp.setPrevivaleCode(userRegResp.getUserRegisterResult().getCodeReturn());
				
				if(userRegResp.getUserRegisterResult().getCodeReturn() == 0) {
					String fullname = usuario.getUsrNombre() + " " + usuario.getUsrApellido() + " " + usuario.getUsrMaterno();
					
					TarjetasUsuario tarjetasUsuario = new TarjetasUsuario();
					tarjetasUsuario.setFechaRegistro(new Date());
					tarjetasUsuario.setIdAplicacion(idApp);
					tarjetasUsuario.setIdFranquicia(MobileCardConstants.WALLET_ID_FRANQUICIA);
					tarjetasUsuario.setIdTarjetasTipo(MobileCardConstants.WALLET_ID_TARJETASTIPO);
					tarjetasUsuario.setIdUsuario(idUsuario);
					tarjetasUsuario.setNumerotarjeta(cardStock.getNumero());
					tarjetasUsuario.setVigencia(cardStock.getVigencia());
					tarjetasUsuario.setCt(cardStock.getCt());
					tarjetasUsuario.setPrevivale(MobileCardConstants.IS_PREVIVALE_TRUE);
					tarjetasUsuario.setNombreTarjeta(fullname);
					
					LOGGER.debug("Guardando la tarjeta en el wallet ...");
					tarjetasUsuario = tarjetasUsuarioRepo.save(tarjetasUsuario);
					LOGGER.debug("Se ha guardo correctamente en el wallet la tarjeta: " + cardStock.getUltimosNumeros());
					
					cardStock.setActiva(MobileCardConstants.PREVIVALE_CARD_STATUS_ACTIVE);
					cardStock.setFechaActivacion(new Date());
					cardStock.setIdUsuario(usuario.getIdUsuario());
					cardStock.setIdEstablecimiento(null);
					cardStock.setIdTarjetasUsuario(tarjetasUsuario.getIdTarjetasUsuario());
					cardStock.setIdTarjetasEstablecimiento(null);
					cardStock.setIdStpCuentasClabe(stpCuentasClabe.getIdStpCuentasClabe());
					
					LOGGER.debug("Actualizando en la BD el stock de las tarjetas Previvale ...");
					cardStock = cardStockRepo.save(cardStock);
					LOGGER.debug("Se ha actualizado correctamente el stock");
					LOGGER.info("El usuario: " + usuario.getUsrLogin() + " tiene asignada la tarjeta: " + cardStock.getUltimosNumeros());
					
					statusCode = StatusConstants.SUCCESS_OPERATION;
					statusMsg = StatusConstants.REGISTER_CLIENT;
					
					baseResp.setIdPrevivaleCardStock(cardStock.getIdPrevivaleCardStock());
				}
				
				long idPeticion = persistReq.saveRequest(PreviValeConstants.SOAP_ACTION_USERREGISTER, classUserReg, baseResp.toString(), usuario.getIdUsuario(), null);
				baseResp.setIdPeticion(idPeticion);
			} else {
				LOGGER.warn("Ya no hay tarjetas disponibles en el stock!");
				return new ApiResponse(StatusConstants.CARD_STOCK_NOT_AVAILABLE, StatusConstants.CARD_STOCK_NOT_AVAILABLE_MSG, baseResp);
			}
		} else {
			LOGGER.error("El usuario esta bloqueado, no se podra registrar la tarjeta");
			return new ApiResponse(StatusConstants.USER_BLOCKED, StatusConstants.USER_BLOCKED_MSG, baseResp);
		}
		
		return new ApiResponse(statusCode, statusMsg, baseResp);
	}
	
	public ApiResponse registroClienteConTarjeta(long idUsuario, String tarjeta, int idApp) {
		BaseResponse baseResp = null;
		int statusCode = StatusConstants.UNSUCCESS_WS_PREVIVALE;
		String statusMsg = StatusConstants.UNSUCCESS_WS_PREVIVALE_MSG;
		
		LOGGER.debug("Buscando en la BD al usuario: " + idUsuario);
		TUsuarios usuario = tUsuariosRepo.findOne(idUsuario);
		
		if(usuario != null && (usuario.getIdUsrStatus() == MobileCardConstants.USUARIO_ACTIVO || 
				usuario.getIdUsrStatus() == MobileCardConstants.USUARIO_ACTIVO_2)) {
			LOGGER.debug("Usuario encontrado: " + usuario.toString());
			LOGGER.info("Buscando en el stock la tarjeta: " + tarjeta);
			
			PreviValeCardStock cardStock = 
					cardStockRepo.findByNumeroAndActivaAndIdEstablecimientoAndIdUsuario(tarjeta, MobileCardConstants.PREVIVALE_CARD_STATUS_ACTIVE, null, null);
			
			if(cardStock != null) {
				tarjeta = AddcelCrypto.decryptTarjeta(tarjeta);
				LOGGER.info("Tarjeta que se va a asignar al usuario: " + cardStock.getUltimosNumeros());
				
				LOGGER.debug("Buscando una cuenta CLABE para asignar a esa tarjeta ...");
				StpCuentasClabe stpCuentasClabe = stpCuentasClabeRepo.findClabeActiva(MobileCardConstants.CLABE_ACTIVA);
				
				if(stpCuentasClabe != null) {
					LOGGER.debug("Se ha encontrado la siguiente cuenta CLABE para asignar: " + stpCuentasClabe.toString());
				} else {
					LOGGER.warn("No se ha encontrado ninguna cuenta CLABE activa para asignar a la tarjeta de PreviVale,"
							+ "verificar que haya CLABEs activas y suficientes en la tabla de stp_cuentas_clabe");
					return new ApiResponse(StatusConstants.CLABE_NOTFOUND, StatusConstants.CLABE_NOTFOUND_MSG, baseResp);
				}
				
				ClassUserRegister classUserReg = new ClassUserRegister();
				classUserReg.setCard(tarjeta);
				classUserReg.setName(usuario.getUsrNombre());
				classUserReg.setLastName(usuario.getUsrApellido());
				classUserReg.setMothersLastName(usuario.getUsrMaterno());
				classUserReg.setCurp(usuario.getUsrCurp());
				classUserReg.setRfc(usuario.getUsrRfc());
				classUserReg.setColony(usuario.getUsrColonia());
				classUserReg.setCp(usuario.getUsrCp());
				classUserReg.setMunicipality(usuario.getUsrCiudad());
				classUserReg.setStreet(usuario.getUsrDireccion());
				classUserReg.setState(obtenerEstado(estadosRepo.findByIdedo(usuario.getUsrIdEstado()).getNombre()));
				classUserReg.setFolio(foliador.generarFolio());
				
				UserRegister userReg = new UserRegister();
				userReg.setUserRegister(classUserReg);
				
				UserRegisterResponse userRegResp = null;
				
				try {
					userRegResp = (UserRegisterResponse) soapConnect.callWebService(userReg, PreviValeConstants.SOAP_ACTION_USERREGISTER);
				} catch(Exception ex) {
					LOGGER.error("Error al consumir el WS de PreviVale: " + ex.getMessage());
					ex.printStackTrace();
					return new ApiResponse(StatusConstants.ERROR_WS_PREVIVALE, StatusConstants.ERROR_WS_PREVIVALE_MSG, baseResp);
				}
				
				LOGGER.debug("Auth Resp: " + userRegResp.getUserRegisterResult().getAuthorization());
				LOGGER.debug("Code Resp: " + userRegResp.getUserRegisterResult().getCodeReturn());
				
				baseResp = new BaseResponse();
				baseResp.setAuthorization(userRegResp.getUserRegisterResult().getAuthorization());
				baseResp.setPrevivaleCode(userRegResp.getUserRegisterResult().getCodeReturn());
				
				if(userRegResp.getUserRegisterResult().getCodeReturn() == 0) {
					String fullname = usuario.getUsrNombre() + " " + usuario.getUsrApellido() + " " + usuario.getUsrMaterno();
					
					TarjetasUsuario tarjetasUsuario = new TarjetasUsuario();
					tarjetasUsuario.setFechaRegistro(new Date());
					tarjetasUsuario.setIdAplicacion(idApp);
					tarjetasUsuario.setIdFranquicia(MobileCardConstants.WALLET_ID_FRANQUICIA);
					tarjetasUsuario.setIdTarjetasTipo(MobileCardConstants.WALLET_ID_TARJETASTIPO);
					tarjetasUsuario.setIdUsuario(idUsuario);
					tarjetasUsuario.setNumerotarjeta(cardStock.getNumero());
					tarjetasUsuario.setVigencia(cardStock.getVigencia());
					tarjetasUsuario.setCt(cardStock.getCt());
					tarjetasUsuario.setPrevivale(MobileCardConstants.IS_PREVIVALE_TRUE);
					tarjetasUsuario.setNombreTarjeta(fullname);
					
					LOGGER.debug("Guardando la tarjeta en el wallet ...");
					tarjetasUsuario = tarjetasUsuarioRepo.save(tarjetasUsuario);
					LOGGER.info("Se ha guardo correctamente en el wallet la tarjeta: " + cardStock.getUltimosNumeros());
					
					cardStock.setActiva(MobileCardConstants.PREVIVALE_CARD_STATUS_ACTIVE);
					cardStock.setFechaActivacion(new Date());
					cardStock.setIdUsuario(usuario.getIdUsuario());
					cardStock.setIdEstablecimiento(null);
					cardStock.setIdTarjetasUsuario(tarjetasUsuario.getIdTarjetasUsuario());
					cardStock.setIdTarjetasEstablecimiento(null);
					cardStock.setIdStpCuentasClabe(stpCuentasClabe.getIdStpCuentasClabe());
					
					LOGGER.debug("Actualizando en la BD el stock de las tarjetas Previvale ...");
					cardStock = cardStockRepo.save(cardStock);
					LOGGER.debug("Se ha actualizado correctamente el stock");
					LOGGER.info("El usuario: " + usuario.getUsrLogin() + " tiene asignada la tarjeta: " + cardStock.getUltimosNumeros());
					
					statusCode = StatusConstants.SUCCESS_OPERATION;
					statusMsg = StatusConstants.REGISTER_CLIENT;
				}
				
				long idPeticion = persistReq.saveRequest(PreviValeConstants.SOAP_ACTION_USERREGISTER, classUserReg, baseResp.toString(), usuario.getIdUsuario(), null);
				baseResp.setIdPeticion(idPeticion);
			} else {
				LOGGER.warn("Esa tarjeta no existe en el stock o ya esta activa!");
				return new ApiResponse(StatusConstants.CARD_STOCK_NOT_AVAILABLE, StatusConstants.CARD_STOCK_NOT_AVAILABLE_MSG, baseResp);
			}
		} else {
			LOGGER.error("El usuario esta bloqueado, no se podra registrar la tarjeta");
			return new ApiResponse(StatusConstants.USER_BLOCKED, StatusConstants.USER_BLOCKED_MSG, baseResp);
		}
		
		return new ApiResponse(statusCode, statusMsg, baseResp);
	}
	
	public int obtenerEstado(String edoNombre) {
		List<Estados> estadosMex = estadosRepo.findMexicoEstados();
		
		int cont = 1;
		for(Estados estado : estadosMex) {
			if(estado.getNombre().equals(edoNombre)) break;
			cont++;
		}
		
		LOGGER.debug("Nombre Estado: " + edoNombre + " Indice: " + cont);
		return cont;
	}

}
