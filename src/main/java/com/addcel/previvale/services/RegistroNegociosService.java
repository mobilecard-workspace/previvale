/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.services;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.previvale.constants.MobileCardConstants;
import com.addcel.previvale.constants.PreviValeConstants;
import com.addcel.previvale.constants.StatusConstants;
import com.addcel.previvale.domain.Estados;
import com.addcel.previvale.domain.LcpfEstablecimiento;
import com.addcel.previvale.domain.PreviValeCardStock;
import com.addcel.previvale.domain.StpCuentasClabe;
import com.addcel.previvale.domain.TarjetasEstablecimiento;
import com.addcel.previvale.repositories.EstadosRepository;
import com.addcel.previvale.repositories.LcpfEstableciemientoRepository;
import com.addcel.previvale.repositories.PreviValeCardStockRepository;
import com.addcel.previvale.repositories.StpCuentasClabeRepository;
import com.addcel.previvale.repositories.TarjetasEstablecimientoRepository;
import com.addcel.previvale.response.ApiResponse;
import com.addcel.previvale.response.BaseResponse;
import com.addcel.previvale.util.Foliador;
import com.addcel.previvale.util.PersistRequest;
import com.addcel.previvale.ws.SOAPConnector;
import com.addcel.utils.AddcelCrypto;

import previvale.wsdl.ClassUserRegister;
import previvale.wsdl.UserRegister;
import previvale.wsdl.UserRegisterResponse;

@Service
public class RegistroNegociosService {

	@Autowired
	private LcpfEstableciemientoRepository lcpfEstableciemientoRepo;
	
	@Autowired
	private EstadosRepository estadosRepo;
	
	@Autowired
	private TarjetasEstablecimientoRepository tarjetasEstablecimientoRepo;
	
	@Autowired
	private PreviValeCardStockRepository cardStockRepo;
	
	@Autowired
	private StpCuentasClabeRepository stpCuentasClabeRepo;
	
	@Autowired
	private PersistRequest persistReq;
				
	@Autowired
	private SOAPConnector soapConnect;
	
	@Autowired
	private Foliador foliador;
	
	private static final Logger LOGGER = LogManager.getLogger(RegistroNegociosService.class);
	
	public ApiResponse registroSinTarjeta(long idEstablecimiento, int idApp) {
		BaseResponse baseResp = null;
		int statusCode = StatusConstants.UNSUCCESS_WS_PREVIVALE;
		String statusMsg = StatusConstants.UNSUCCESS_WS_PREVIVALE_MSG;
		
		LOGGER.debug("Buscando en la BD al negocio: " + idEstablecimiento);
		
		LcpfEstablecimiento establecimiento = lcpfEstableciemientoRepo.findOne(idEstablecimiento);
		
		if(establecimiento != null && (establecimiento.getEstatus() == MobileCardConstants.USUARIO_ACTIVO || 
				establecimiento.getEstatus() == MobileCardConstants.USUARIO_ACTIVO_2)) {
			LOGGER.debug("Establecimiento encontrado: " + establecimiento.toString());
			LOGGER.info("Buscando una tarjeta disponible en el sotck ...");
			
			List<PreviValeCardStock> cardStockList = 
					cardStockRepo.findFirst1ByActivaAndIdUsuarioAndIdEstablecimiento(MobileCardConstants.PREVIVALE_CARD_STATUS_INACTIVE, null, null);
			
			if(cardStockList != null && cardStockList.size() > 0) {
				PreviValeCardStock cardStock = cardStockList.get(0);
				String tarjeta = AddcelCrypto.decryptTarjeta(cardStock.getNumero());
				LOGGER.info("Tarjeta econtrada para asignar al usuario: " + cardStock.getUltimosNumeros());
				
				LOGGER.debug("Buscando una cuenta CLABE para asignar a esa tarjeta ...");
				StpCuentasClabe stpCuentasClabe = stpCuentasClabeRepo.findClabeActiva(MobileCardConstants.CLABE_ACTIVA);
				
				if(stpCuentasClabe != null) {
					LOGGER.debug("Se ha encontrado la siguiente cuenta CLABE para asignar: " + stpCuentasClabe.toString());
				} else {
					LOGGER.warn("No se ha encontrado ninguna cuenta CLABE activa para asignar a la tarjeta de PreviVale,"
							+ "verificar que haya CLABEs activas y suficientes en la tabla de stp_cuentas_clabe");
					return new ApiResponse(StatusConstants.CLABE_NOTFOUND, StatusConstants.CLABE_NOTFOUND_MSG, baseResp);
				}
				
				ClassUserRegister classUserReg = new ClassUserRegister();
				classUserReg.setCard(tarjeta);
				classUserReg.setName(establecimiento.getRepresentanteNombre());
				classUserReg.setLastName(establecimiento.getRepresentantePaterno());
				classUserReg.setMothersLastName(establecimiento.getRepresentanteMaterno());
				classUserReg.setCurp(establecimiento.getRepresentanteCurp());
				classUserReg.setRfc(establecimiento.getRfc());
				classUserReg.setColony(establecimiento.getColonia());
				classUserReg.setCp(establecimiento.getCp());
				classUserReg.setMunicipality(establecimiento.getMunicipio());
				classUserReg.setStreet(establecimiento.getCalle());
				classUserReg.setState(obtenerEstado(estadosRepo.findByIdedo(establecimiento.getIdEstado()).getNombre()));
				classUserReg.setFolio(foliador.generarFolio());
				
				UserRegister userReg = new UserRegister();
				userReg.setUserRegister(classUserReg);
				
				UserRegisterResponse userRegResp = null;
				
				try {
					userRegResp = (UserRegisterResponse) soapConnect.callWebService(userReg, PreviValeConstants.SOAP_ACTION_USERREGISTER);
				} catch(Exception ex) {
					LOGGER.error("Error al consumir el WS de PreviVale: " + ex.getMessage());
					ex.printStackTrace();
					return new ApiResponse(StatusConstants.ERROR_WS_PREVIVALE, StatusConstants.ERROR_WS_PREVIVALE_MSG, baseResp);
				}
				
				LOGGER.debug("Auth Resp: " + userRegResp.getUserRegisterResult().getAuthorization());
				LOGGER.debug("Code Resp: " + userRegResp.getUserRegisterResult().getCodeReturn());
				
				baseResp = new BaseResponse();
				baseResp.setAuthorization(userRegResp.getUserRegisterResult().getAuthorization());
				baseResp.setPrevivaleCode(userRegResp.getUserRegisterResult().getCodeReturn());
				
				if(userRegResp.getUserRegisterResult().getCodeReturn() == 0) {
					String fullnameRepresentante = establecimiento.getRepresentanteNombre() + " " +
							establecimiento.getRepresentantePaterno() + " " + establecimiento.getRepresentanteMaterno();
					
					TarjetasEstablecimiento tarjetaEstablecimiento = new TarjetasEstablecimiento();
					tarjetaEstablecimiento.setCt(cardStock.getCt());
					tarjetaEstablecimiento.setFechaRegistro(new Date());
					tarjetaEstablecimiento.setIdAplicacion(idApp);
					tarjetaEstablecimiento.setIdEstablecimiento(establecimiento.getId());
					tarjetaEstablecimiento.setIdFranquicia(MobileCardConstants.WALLET_ID_FRANQUICIA);
					tarjetaEstablecimiento.setIdTarjetasTipo(MobileCardConstants.WALLET_ID_TARJETASTIPO);
					tarjetaEstablecimiento.setNumeroTarjeta(cardStock.getNumero());
					tarjetaEstablecimiento.setVigencia(cardStock.getVigencia());
					tarjetaEstablecimiento.setEstatus(MobileCardConstants.USUARIO_ACTIVO);
					tarjetaEstablecimiento.setPrevivale(MobileCardConstants.IS_PREVIVALE_TRUE);
					tarjetaEstablecimiento.setNombreTarjeta(fullnameRepresentante);
					
					LOGGER.debug("Guardando la tarjeta en el wallet ...");
					tarjetaEstablecimiento = tarjetasEstablecimientoRepo.save(tarjetaEstablecimiento);
					LOGGER.debug("Se ha guardo correctamente en el wallet la tarjeta: " + cardStock.getUltimosNumeros());
					
					cardStock.setActiva(MobileCardConstants.PREVIVALE_CARD_STATUS_ACTIVE);
					cardStock.setFechaActivacion(new Date());
					cardStock.setIdUsuario(null);
					cardStock.setIdEstablecimiento(idEstablecimiento);
					cardStock.setIdTarjetasUsuario(null);
					cardStock.setIdTarjetasEstablecimiento(tarjetaEstablecimiento.getIdTarjetasEstablecimiento());
					cardStock.setIdStpCuentasClabe(stpCuentasClabe.getIdStpCuentasClabe());
					
					LOGGER.debug("Actualizando en la BD el stock de las tarjetas Previvale ...");
					cardStock = cardStockRepo.save(cardStock);
					LOGGER.debug("Se ha actualizado correctamente el stock");
					LOGGER.info("El negocio: " + idEstablecimiento + " tiene asignada la tarjeta: " + cardStock.getUltimosNumeros());
					
					statusCode = StatusConstants.SUCCESS_OPERATION;
					statusMsg = StatusConstants.REGISTER_CLIENT;
					
					baseResp.setIdPrevivaleCardStock(cardStock.getIdPrevivaleCardStock());
				}
				
				long idPeticion = persistReq.saveRequest(PreviValeConstants.SOAP_ACTION_USERREGISTER, classUserReg, baseResp.toString(), null, establecimiento.getId());
				baseResp.setIdPeticion(idPeticion);
			} else {
				LOGGER.warn("Esa tarjeta no existe en el stock o ya esta activa!");
				return new ApiResponse(StatusConstants.CARD_STOCK_NOT_AVAILABLE, StatusConstants.CARD_STOCK_NOT_AVAILABLE_MSG, baseResp);
			}
		} else {
			LOGGER.error("El negocio esta bloqueado, no se podra registrar la tarjeta");
			return new ApiResponse(StatusConstants.USER_BLOCKED, StatusConstants.USER_BLOCKED_MSG, baseResp);
		}
		
		return new ApiResponse(statusCode, statusMsg, baseResp);
	}
	
	public ApiResponse registroConTarjeta(long idEstablecimiento, String tarjeta, int idApp) {
		BaseResponse baseResp = null;
		int statusCode = StatusConstants.UNSUCCESS_WS_PREVIVALE;
		String statusMsg = StatusConstants.UNSUCCESS_WS_PREVIVALE_MSG;
		
		LOGGER.debug("Buscando en la BD al negocio: " + idEstablecimiento);
		
		LcpfEstablecimiento establecimiento = lcpfEstableciemientoRepo.findOne(idEstablecimiento);
		
		if(establecimiento != null && (establecimiento.getEstatus() == MobileCardConstants.USUARIO_ACTIVO || 
				establecimiento.getEstatus() == MobileCardConstants.USUARIO_ACTIVO_2)) {
			LOGGER.debug("Establecimiento encontrado: " + establecimiento.toString());
			LOGGER.info("Buscando en el stock la tarjeta: " + tarjeta);
			
			PreviValeCardStock cardStock = 
					cardStockRepo.findByNumeroAndActivaAndIdEstablecimientoAndIdUsuario(tarjeta, MobileCardConstants.PREVIVALE_CARD_STATUS_ACTIVE, null, null);
			
			if(cardStock != null) {
				tarjeta = AddcelCrypto.decryptTarjeta(tarjeta);
				LOGGER.info("Tarjeta que se va a asignar al usuario: " + cardStock.getUltimosNumeros());
				
				LOGGER.debug("Buscando una cuenta CLABE para asignar a esa tarjeta ...");
				StpCuentasClabe stpCuentasClabe = stpCuentasClabeRepo.findClabeActiva(MobileCardConstants.CLABE_ACTIVA);
				
				if(stpCuentasClabe != null) {
					LOGGER.debug("Se ha encontrado la siguiente cuenta CLABE para asignar: " + stpCuentasClabe.toString());
				} else {
					LOGGER.warn("No se ha encontrado ninguna cuenta CLABE activa para asignar a la tarjeta de PreviVale,"
							+ "verificar que haya CLABEs activas y suficientes en la tabla de stp_cuentas_clabe");
					return new ApiResponse(StatusConstants.CLABE_NOTFOUND, StatusConstants.CLABE_NOTFOUND_MSG, baseResp);
				}
				
				ClassUserRegister classUserReg = new ClassUserRegister();
				classUserReg.setCard(tarjeta);
				classUserReg.setName(establecimiento.getRepresentanteNombre());
				classUserReg.setLastName(establecimiento.getRepresentantePaterno());
				classUserReg.setMothersLastName(establecimiento.getRepresentanteMaterno());
				classUserReg.setCurp(establecimiento.getRepresentanteCurp());
				classUserReg.setRfc(establecimiento.getRfc());
				classUserReg.setColony(establecimiento.getColonia());
				classUserReg.setCp(establecimiento.getCp());
				classUserReg.setMunicipality(establecimiento.getMunicipio());
				classUserReg.setStreet(establecimiento.getCalle());
				classUserReg.setState(obtenerEstado(estadosRepo.findByIdedo(establecimiento.getIdEstado()).getNombre()));
				classUserReg.setFolio(foliador.generarFolio());
				
				UserRegister userReg = new UserRegister();
				userReg.setUserRegister(classUserReg);
				
				UserRegisterResponse userRegResp = null;
				
				try {
					userRegResp = (UserRegisterResponse) soapConnect.callWebService(userReg, PreviValeConstants.SOAP_ACTION_USERREGISTER);
				} catch(Exception ex) {
					LOGGER.error("Error al consumir el WS de PreviVale: " + ex.getMessage());
					ex.printStackTrace();
					return new ApiResponse(StatusConstants.ERROR_WS_PREVIVALE, StatusConstants.ERROR_WS_PREVIVALE_MSG, baseResp);
				}
				
				LOGGER.debug("Auth Resp: " + userRegResp.getUserRegisterResult().getAuthorization());
				LOGGER.debug("Code Resp: " + userRegResp.getUserRegisterResult().getCodeReturn());
				
				baseResp = new BaseResponse();
				baseResp.setAuthorization(userRegResp.getUserRegisterResult().getAuthorization());
				baseResp.setPrevivaleCode(userRegResp.getUserRegisterResult().getCodeReturn());
				//baseResp.setAuthorization("OK");
				//baseResp.setPrevivaleCode(0);
				if(userRegResp.getUserRegisterResult().getCodeReturn() == 0) {
				//if(0 == 0) {
					String fullnameRepresentante = establecimiento.getRepresentanteNombre() + " " +
							establecimiento.getRepresentantePaterno() + " " + establecimiento.getRepresentanteMaterno();
					
					TarjetasEstablecimiento tarjetaEstablecimiento = new TarjetasEstablecimiento();
					tarjetaEstablecimiento.setCt(cardStock.getCt());
					tarjetaEstablecimiento.setFechaRegistro(new Date());
					tarjetaEstablecimiento.setIdAplicacion(idApp);
					tarjetaEstablecimiento.setIdEstablecimiento(establecimiento.getId());
					tarjetaEstablecimiento.setIdFranquicia(MobileCardConstants.WALLET_ID_FRANQUICIA);
					tarjetaEstablecimiento.setIdTarjetasTipo(MobileCardConstants.WALLET_ID_TARJETASTIPO);
					tarjetaEstablecimiento.setNumeroTarjeta(cardStock.getNumero());
					tarjetaEstablecimiento.setVigencia(cardStock.getVigencia());
					tarjetaEstablecimiento.setEstatus(MobileCardConstants.USUARIO_ACTIVO);
					tarjetaEstablecimiento.setPrevivale(MobileCardConstants.IS_PREVIVALE_TRUE);
					tarjetaEstablecimiento.setNombreTarjeta(fullnameRepresentante);
					
					LOGGER.debug("Guardando la tarjeta en el wallet ...");
					tarjetaEstablecimiento = tarjetasEstablecimientoRepo.save(tarjetaEstablecimiento);
					LOGGER.debug("Se ha guardo correctamente en el wallet la tarjeta: " + cardStock.getUltimosNumeros());
					
					cardStock.setActiva(MobileCardConstants.PREVIVALE_CARD_STATUS_ACTIVE);
					cardStock.setFechaActivacion(new Date());
					cardStock.setIdUsuario(null);
					cardStock.setIdEstablecimiento(idEstablecimiento);
					cardStock.setIdTarjetasUsuario(null);
					cardStock.setIdTarjetasEstablecimiento(tarjetaEstablecimiento.getIdTarjetasEstablecimiento());
					cardStock.setIdStpCuentasClabe(stpCuentasClabe.getIdStpCuentasClabe());
					
					LOGGER.debug("Actualizando en la BD el stock de las tarjetas Previvale ...");
					cardStock = cardStockRepo.save(cardStock);
					LOGGER.debug("Se ha actualizado correctamente el stock");
					LOGGER.info("El negocio: " + idEstablecimiento + " tiene asignada la tarjeta: " + cardStock.getUltimosNumeros());
					
					statusCode = StatusConstants.SUCCESS_OPERATION;
					statusMsg = StatusConstants.REGISTER_CLIENT;
				}
				
				long idPeticion = persistReq.saveRequest(PreviValeConstants.SOAP_ACTION_USERREGISTER, classUserReg, baseResp.toString(), null, establecimiento.getId());
				baseResp.setIdPeticion(idPeticion);
			} else {
				LOGGER.warn("Esa tarjeta no existe en el stock o ya esta activa!");
				return new ApiResponse(StatusConstants.CARD_STOCK_NOT_AVAILABLE, StatusConstants.CARD_STOCK_NOT_AVAILABLE_MSG, baseResp);
			}
		} else {
			LOGGER.error("El negocio esta bloqueado, no se podra registrar la tarjeta");
			return new ApiResponse(StatusConstants.USER_BLOCKED, StatusConstants.USER_BLOCKED_MSG, baseResp);
		}
		
		return new ApiResponse(statusCode, statusMsg, baseResp);
	}
	
	public int obtenerEstado(String edoNombre) {
		List<Estados> estadosMex = estadosRepo.findMexicoEstados();
		
		int cont = 1;
		for(Estados estado : estadosMex) {
			if(estado.getNombre().equals(edoNombre)) break;
			cont++;
		}
		
		LOGGER.debug("Nombre Estado: " + edoNombre + " Indice: " + cont);
		return cont;
	}
	
}
