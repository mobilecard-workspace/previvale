/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.previvale.constants.MobileCardConstants;
import com.addcel.previvale.constants.PreviValeConstants;
import com.addcel.previvale.constants.StatusConstants;
import com.addcel.previvale.domain.LcpfEstablecimiento;
import com.addcel.previvale.domain.TarjetasEstablecimiento;
import com.addcel.previvale.repositories.LcpfEstableciemientoRepository;
import com.addcel.previvale.repositories.TarjetasEstablecimientoRepository;
import com.addcel.previvale.response.ApiResponse;
import com.addcel.previvale.response.EstatusTarjetaNegocioResponse;
import com.addcel.previvale.ws.SOAPConnector;
import com.addcel.utils.AddcelCrypto;

import previvale.wsdl.ClassGetCreditLimit;
import previvale.wsdl.GetCreditLimitAndAvailableData;
import previvale.wsdl.GetCreditLimitAndAvailableDataResponse;

@Service
public class EstatusNegocioService {
	
	@Autowired
	private TarjetasEstablecimientoRepository tarjetasEstablecimientoRepo;
	
	@Autowired
	private LcpfEstableciemientoRepository establecimientoRepo;
	
	@Autowired
	private SOAPConnector soapConnect;

	private static final Logger LOGGER = LogManager.getLogger(EstatusNegocioService.class);
	
	public ApiResponse estatusTarjetaNegocio(long idEstablecimiento) {
		int statusCode = StatusConstants.UNSUCCESS_WS_PREVIVALE;
		String statusMsg = StatusConstants.NEGOCIO_NOTFOUND_MSG;
		EstatusTarjetaNegocioResponse estatusNegocioResp = null;
		
		LOGGER.debug("Buscando en la BD el establecimiento: " + idEstablecimiento);
		LcpfEstablecimiento establecimiento = establecimientoRepo.findOne(idEstablecimiento);
		
		if(establecimiento != null && (establecimiento.getEstatus() == MobileCardConstants.USUARIO_ACTIVO ||
				establecimiento.getEstatus() == MobileCardConstants.USUARIO_ACTIVO_2)) {
			TarjetasEstablecimiento establecimientoWallet = tarjetasEstablecimientoRepo.findByIdEstablecimiento(idEstablecimiento);
			
			if(establecimientoWallet != null) {
				LOGGER.debug("Tarjeta del establecimiento encontrada: " + establecimientoWallet.getNumeroTarjeta());
				
				ClassGetCreditLimit classGetCredit = new ClassGetCreditLimit();
				classGetCredit.setCard(AddcelCrypto.decryptTarjeta(establecimientoWallet.getNumeroTarjeta()));
				GetCreditLimitAndAvailableData getCredit = new GetCreditLimitAndAvailableData();
				getCredit.setGetCreditLimit(classGetCredit);
				
				GetCreditLimitAndAvailableDataResponse getCreditResp = null;
				
				try {
					getCreditResp = (GetCreditLimitAndAvailableDataResponse) soapConnect.callWebService(getCredit, 
							PreviValeConstants.SOAP_ACTION_GETCREDITLIMIT);
				} catch(Exception ex) {
					LOGGER.error("Error al consumir el WS de PreviVale: " + ex.getMessage());
					ex.printStackTrace();
					return new ApiResponse(StatusConstants.UNSUCCESS_WS_PREVIVALE, StatusConstants.UNSUCCESS_WS_PREVIVALE_MSG, null);
				}
				
				LOGGER.debug("Auth Resp: " + getCreditResp.getGetCreditLimitAndAvailableDataResult().getAuthorization());
				LOGGER.debug("Code Resp: " + getCreditResp.getGetCreditLimitAndAvailableDataResult().getCodeReturn());
				
				if(getCreditResp.getGetCreditLimitAndAvailableDataResult().getCodeReturn() == 0) {
					statusCode = StatusConstants.SUCCESS_OPERATION;
					statusMsg = StatusConstants.STATUS_CARD_NEGOCIO_MSG;
					
					estatusNegocioResp = new EstatusTarjetaNegocioResponse();
					estatusNegocioResp.setCt(establecimientoWallet.getCt());
					estatusNegocioResp.setDeterminada(true);
					estatusNegocioResp.setFechaRegistro(establecimientoWallet.getFechaRegistro());
					estatusNegocioResp.setPan(establecimientoWallet.getNumeroTarjeta());
					estatusNegocioResp.setTipoTarjeta(establecimientoWallet.getIdTarjetasTipo());
					estatusNegocioResp.setVigencia(establecimientoWallet.getVigencia());
					estatusNegocioResp.setBalance(Double.parseDouble(getCreditResp.getGetCreditLimitAndAvailableDataResult().getAuthorization()));
					String representante = establecimiento.getRepresentanteNombre() + " " + establecimiento.getRepresentantePaterno() + 
							" " + establecimiento.getRepresentanteMaterno();
					estatusNegocioResp.setRepresentanteLegal(representante);
				} else {
					LOGGER.warn("La respuesta del WS de PreviVale no fue exitosa");
					
				}
			} else {
				LOGGER.warn("El establecimiento no tiene tarjeta asignada en el wallet");
				statusCode = StatusConstants.WALLET_NOTFOUND;
				statusMsg = StatusConstants.WALLET_NOTFOUND_MSG;
			}
		} else {
			LOGGER.warn("El establecimiento no existe o est� inactivo");
		}
		
		return new ApiResponse(statusCode, statusMsg, estatusNegocioResp);
	}
	
}
