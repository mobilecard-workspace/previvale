/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.services;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.previvale.constants.MobileCardConstants;
import com.addcel.previvale.constants.PreviValeConstants;
import com.addcel.previvale.constants.StatusConstants;
import com.addcel.previvale.response.ApiResponse;
import com.addcel.previvale.response.BaseResponse;
import com.addcel.previvale.util.PersistRequest;
import com.addcel.previvale.util.ValidarUsuarios;
import com.addcel.previvale.ws.SOAPConnector;
import com.addcel.utils.AddcelCrypto;

import previvale.wsdl.ClassGetStatus;
import previvale.wsdl.GetStatus;
import previvale.wsdl.GetStatusResponse;
import previvale.wsdl.SetUnlock;
import previvale.wsdl.SetUnlockResponse;
import previvale.wsdl.Setlock;
import previvale.wsdl.SetlockResponse;

@Service
public class EstatusTarjetaService {
	
	@Autowired
	private PersistRequest persistReq;
	
	@Autowired
	private SOAPConnector soapConnect;
	
	@Autowired
	private ValidarUsuarios validarUsuarios;
	
	private static final Logger LOGGER = LogManager.getLogger(EstatusTarjetaService.class);
	
	public ApiResponse bloqueoTarjeta(Long idUsuario, Long idEstablecimiento, String tarjeta) {
		BaseResponse baseResp = null;
		int statusCode = StatusConstants.UNSUCCESS_WS_PREVIVALE;
		String statusMsg = StatusConstants.UNSUCCESS_WS_PREVIVALE_MSG;
		
		Map<String, Object> map = validarUsuarios.getUserStatus(idUsuario, idEstablecimiento);
		
		if((Integer) map.get("status") == MobileCardConstants.USUARIO_ACTIVO) {
			LOGGER.debug("Usuario encontrado y activo!");
			
			tarjeta = AddcelCrypto.decryptTarjeta(tarjeta);
			
			SetlockResponse setLockResp = null;
			Setlock setLock = new Setlock();
			setLock.setCard(tarjeta);
			
			try {
				setLockResp = (SetlockResponse) soapConnect.callWebService(setLock, PreviValeConstants.SOAP_ACTION_SETLOCK);
			} catch(Exception ex) {
				LOGGER.error("Error al consumir el WS de PreviVale: " + ex.getMessage());
				ex.printStackTrace();
				return new ApiResponse(StatusConstants.ERROR_WS_PREVIVALE, StatusConstants.ERROR_WS_PREVIVALE_MSG, baseResp);
			}
						
			LOGGER.debug("Auth Resp: " + setLockResp.getSetlockResult().getAuthorization());
			LOGGER.debug("Code Resp: " + setLockResp.getSetlockResult().getCodeReturn());
			
			baseResp = new BaseResponse();
			baseResp.setAuthorization(setLockResp.getSetlockResult().getAuthorization());
			baseResp.setPrevivaleCode(setLockResp.getSetlockResult().getCodeReturn());
			
			if(setLockResp.getSetlockResult().getCodeReturn() == 0) {
				statusCode = StatusConstants.SUCCESS_OPERATION;
				statusMsg = StatusConstants.BLOCK_CARD_MSG;
			}
			
			long idPeticion = persistReq.saveRequest(PreviValeConstants.SOAP_ACTION_SETLOCK, setLock, baseResp.toString(), idUsuario, idEstablecimiento);
			baseResp.setIdPeticion(idPeticion);
		} else {
			LOGGER.error("El usuario esta bloqueado, no se podra bloquear la tarjeta");
			return new ApiResponse(StatusConstants.USER_BLOCKED, StatusConstants.USER_BLOCKED_MSG, baseResp);
		}
		
		return new ApiResponse(statusCode, statusMsg, baseResp);
	}
	
	public ApiResponse activacionTarjeta(Long idUsuario, Long idEstablecimiento, String tarjeta) {
		BaseResponse baseResp = null;
		int statusCode = StatusConstants.UNSUCCESS_WS_PREVIVALE;
		String statusMsg = StatusConstants.UNSUCCESS_WS_PREVIVALE_MSG;
		
		Map<String, Object> map = validarUsuarios.getUserStatus(idUsuario, idEstablecimiento);
		
		if((Integer) map.get("status") == MobileCardConstants.USUARIO_ACTIVO) {
			LOGGER.debug("Usuario encontrado y activo!");
			
			tarjeta = AddcelCrypto.decryptTarjeta(tarjeta);
			
			SetUnlockResponse setUnlockResp = null;
			SetUnlock setUnlock = new SetUnlock();
			setUnlock.setCard(tarjeta);
			
			try {
				setUnlockResp = (SetUnlockResponse) soapConnect.callWebService(setUnlock, PreviValeConstants.SOAP_ACTION_SETUNLOCK);
			} catch(Exception ex) {
				LOGGER.error("Error al consumir el WS de PreviVale: " + ex.getMessage());
				ex.printStackTrace();
				return new ApiResponse(StatusConstants.ERROR_WS_PREVIVALE, StatusConstants.ERROR_WS_PREVIVALE_MSG, baseResp);
			}
						
			LOGGER.debug("Auth Resp: " + setUnlockResp.getSetUnlockResult().getAuthorization());
			LOGGER.debug("Code Resp: " + setUnlockResp.getSetUnlockResult().getCodeReturn());
			
			baseResp = new BaseResponse();
			baseResp.setAuthorization(setUnlockResp.getSetUnlockResult().getAuthorization());
			baseResp.setPrevivaleCode(setUnlockResp.getSetUnlockResult().getCodeReturn());
			
			if(setUnlockResp.getSetUnlockResult().getCodeReturn() == 0) {
				statusCode = StatusConstants.SUCCESS_OPERATION;
				statusMsg = StatusConstants.ACTIVATE_CARD_MSG;
			}
			
			long idPeticion = persistReq.saveRequest(PreviValeConstants.SOAP_ACTION_SETUNLOCK, setUnlock, baseResp.toString(), idUsuario, idEstablecimiento);
			baseResp.setIdPeticion(idPeticion);
		} else {
			LOGGER.error("El usuario esta bloqueado, no se podra activar la tarjeta");
			return new ApiResponse(StatusConstants.USER_BLOCKED, StatusConstants.USER_BLOCKED_MSG, baseResp);
		}
		
		return new ApiResponse(statusCode, statusMsg, baseResp);
	}
	
	public ApiResponse estatusTarjeta(Long idUsuario, Long idEstablecimiento, String tarjeta) {
		BaseResponse baseResp = null;
		int statusCode = StatusConstants.UNSUCCESS_WS_PREVIVALE;
		String statusMsg = StatusConstants.UNSUCCESS_WS_PREVIVALE_MSG;
		
		Map<String, Object> map = validarUsuarios.getUserStatus(idUsuario, idEstablecimiento);
		
		if((Integer) map.get("status") == MobileCardConstants.USUARIO_ACTIVO || 
				(Integer) map.get("status") == MobileCardConstants.USUARIO_ACTIVO_2) {
			LOGGER.debug("Usuario encontrado y activo!");
			
			tarjeta = AddcelCrypto.decryptTarjeta(tarjeta);
			
			GetStatusResponse getStatusResp = null;
			ClassGetStatus classGetStatus = new ClassGetStatus();
			classGetStatus.setCard(tarjeta);
			GetStatus getStatus = new GetStatus();
			getStatus.setGetStatus(classGetStatus);
			
			try {
				getStatusResp = (GetStatusResponse) soapConnect.callWebService(getStatus, PreviValeConstants.SOAP_ACTION_GETSTATUS);
			} catch(Exception ex) {
				LOGGER.error("Error al consumir el WS de PreviVale: " + ex.getMessage());
				ex.printStackTrace();
				return new ApiResponse(StatusConstants.ERROR_WS_PREVIVALE, StatusConstants.ERROR_WS_PREVIVALE_MSG, baseResp);
			}
			
			LOGGER.debug("Auth Resp: " + getStatusResp.getGetStatusResult().getAuthorization());
			LOGGER.debug("Code Resp: " + getStatusResp.getGetStatusResult().getCodeReturn());
			
			baseResp = new BaseResponse();
			baseResp.setAuthorization(getStatusResp.getGetStatusResult().getAuthorization());
			baseResp.setPrevivaleCode(getStatusResp.getGetStatusResult().getCodeReturn());
			
			if(getStatusResp.getGetStatusResult().getCodeReturn() == 0) {
				statusCode = StatusConstants.SUCCESS_OPERATION;
				statusMsg = StatusConstants.STATUS_CARD_MSG;
			}
			
			long idPeticion = persistReq.saveRequest(PreviValeConstants.SOAP_ACTION_GETSTATUS, classGetStatus, baseResp.toString(), idUsuario, idEstablecimiento);
			baseResp.setIdPeticion(idPeticion);
		} else {
			LOGGER.error("El usuario esta bloqueado, no se podra obtener el estatus la tarjeta");
			return new ApiResponse(StatusConstants.USER_BLOCKED, StatusConstants.USER_BLOCKED_MSG, baseResp);
		}
		
		return new ApiResponse(statusCode, statusMsg, baseResp);
	}
	
}