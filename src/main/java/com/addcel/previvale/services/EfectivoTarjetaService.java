/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.services;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import com.addcel.previvale.constants.MobileCardConstants;
import com.addcel.previvale.constants.PreviValeConstants;
import com.addcel.previvale.constants.PushConstants;
import com.addcel.previvale.constants.StatusConstants;
import com.addcel.previvale.response.ApiResponse;
import com.addcel.previvale.response.BaseResponse;
import com.addcel.previvale.util.Foliador;
import com.addcel.previvale.util.PersistRequest;
import com.addcel.previvale.util.PropertiesFile;
import com.addcel.previvale.util.RestClient;
import com.addcel.previvale.util.ValidarUsuarios;
import com.addcel.previvale.ws.PushParams;
import com.addcel.previvale.ws.PushRequest;
import com.addcel.previvale.ws.PushResponse;
import com.addcel.previvale.ws.SOAPConnector;
import com.addcel.utils.AddcelCrypto;

import previvale.wsdl.ClassSetOnlinePayment;
import previvale.wsdl.OnlinePayment;
import previvale.wsdl.OnlinePaymentResponse;
import previvale.wsdl.SetDebitcard;
import previvale.wsdl.SetDebitcardResponse;

@Service
public class EfectivoTarjetaService {
	
	@Autowired
	private PersistRequest persistReq;
	
	@Autowired
	private SOAPConnector soapConnect;
	
	@Autowired
	private Foliador foliador;
	
	@Autowired
	private PropertiesFile propsFile;
	
	@Autowired
	private ValidarUsuarios validarUsuarios;
	
	@Autowired
	private RestClient restClient;
	
	private static final Logger LOGGER = LogManager.getLogger(EfectivoTarjetaService.class);
	
	public ApiResponse agregaDinero(Long idUsuario, Long idEstablecimiento, String tarjeta, Double cantidad,
			int idApp, int idPais, String idioma) {
		BaseResponse baseResp = null;
		int statusCode = StatusConstants.UNSUCCESS_WS_PREVIVALE;
		String statusMsg = StatusConstants.UNSUCCESS_WS_PREVIVALE_MSG;
		Long idUser = idUsuario != null ? idUsuario : idEstablecimiento;
		String tipoUsuario = idUsuario != null ? PushConstants.PUSH_TIPO_USUARIO : PushConstants.PUSH_TIPO_NEGOCIO;
		DecimalFormat df2 = new DecimalFormat("#.##");
				
		Map<String, Object> map = validarUsuarios.getUserStatus(idUsuario, idEstablecimiento);
		
		if((Integer) map.get("status") == MobileCardConstants.USUARIO_ACTIVO) {
			LOGGER.debug("Usuario encontrado y activo!");
			
			tarjeta = AddcelCrypto.decryptTarjeta(tarjeta);
			
			ClassSetOnlinePayment classSetOnlinePay = new ClassSetOnlinePayment();
			classSetOnlinePay.setAmount(cantidad);
			classSetOnlinePay.setCard(tarjeta);
			classSetOnlinePay.setClient(propsFile.getPrevivaleWsClient());
			classSetOnlinePay.setFolio(foliador.generarFolio());
			OnlinePayment onlinePay = new OnlinePayment();
			onlinePay.setOnlinePayment(classSetOnlinePay);
			
			OnlinePaymentResponse onlinePayResp = null;
			
			try {
				onlinePayResp = (OnlinePaymentResponse) soapConnect.callWebService(onlinePay, PreviValeConstants.SOAP_ACTION_ONLINEPAYMENT);
			} catch(Exception ex) {
				LOGGER.error("Error al consumir el WS de PreviVale: " + ex.getMessage());
				ex.printStackTrace();
				return new ApiResponse(StatusConstants.ERROR_WS_PREVIVALE, StatusConstants.ERROR_WS_PREVIVALE_MSG, baseResp);
			}
			
			LOGGER.debug("Auth Resp: " + onlinePayResp.getOnlinePaymentResult().getAuthorization());
			LOGGER.debug("Code Resp: " + onlinePayResp.getOnlinePaymentResult().getCodeReturn());
			
			baseResp = new BaseResponse();
			baseResp.setAuthorization(onlinePayResp.getOnlinePaymentResult().getAuthorization());
			baseResp.setPrevivaleCode(onlinePayResp.getOnlinePaymentResult().getCodeReturn());
			
			if(onlinePayResp.getOnlinePaymentResult().getCodeReturn() == 0) {
				LOGGER.info("Se ha agregado correctamente el dinero a la tarjeta");
				statusCode = StatusConstants.SUCCESS_OPERATION;
				statusMsg = StatusConstants.ADD_CASH_MSG;
				
				String tarjetaMask = tarjeta.substring(0, 4) + " **** **** " + tarjeta.substring(12, 16);
				
				PushParams[] params = {new PushParams(PushConstants.PARAM_NOMBRES, (String) map.get("fullname")), 
						new PushParams(PushConstants.PARAM_MONTO, df2.format(cantidad)),
						new PushParams(PushConstants.PARAM_TARJETA, tarjetaMask)};
				
				PushRequest pushReq = new PushRequest();
				pushReq.setId_usuario(idUser);
				pushReq.setIdApp(idApp);
				pushReq.setIdioma(idioma);
				pushReq.setIdPais(idPais);
				pushReq.setModulo(propsFile.getPushModuloPrevivale());
				pushReq.setTipoUsuario(tipoUsuario);
				pushReq.setParams(params);
				
				try {
					LOGGER.info("Enviando notificacion push ...");
					PushResponse pushResp = restClient.createRequest(pushReq, propsFile.getPushServiceUrl(), HttpMethod.POST);
					
					if(pushResp.getCode() == PushConstants.PUSH_SUCCESS) {
						LOGGER.info("Se envio correctamente la PUSH");
					} else {
						LOGGER.error("No se envio la PUSH, la API regreso el siguiente error: {} - {}", pushResp.getCode(), pushResp.getMessage());
					}
				} catch (RestClientException | IOException ex) {
					LOGGER.error("Ocurrio un error con la api de notificaciones PUSH: " + ex.getMessage());
					ex.printStackTrace();
				}
			}
			
			long idPeticion = persistReq.saveRequest(PreviValeConstants.SOAP_ACTION_ONLINEPAYMENT, classSetOnlinePay, baseResp.toString(), idUsuario, idEstablecimiento);
			baseResp.setIdPeticion(idPeticion);
		} else {
			LOGGER.error("El usuario esta bloqueado, no se podra agregar dinero a la tarjeta");
			return new ApiResponse(StatusConstants.USER_BLOCKED, StatusConstants.USER_BLOCKED_MSG, baseResp);
		}
		
		return new ApiResponse(statusCode, statusMsg, baseResp);
	}
	
	public ApiResponse retiraDinero(Long idUsuario, Long idEstablecimiento, String tarjeta, Double cantidad, String validez) {
		BaseResponse baseResp = null;
		int statusCode = StatusConstants.UNSUCCESS_WS_PREVIVALE;
		String statusMsg = StatusConstants.UNSUCCESS_WS_PREVIVALE_MSG;
		
		Map<String, Object> map = validarUsuarios.getUserStatus(idUsuario, idEstablecimiento);
		
		if((Integer) map.get("status") == MobileCardConstants.USUARIO_ACTIVO) {
			LOGGER.debug("Usuario encontrado y activo!");
			
			tarjeta = AddcelCrypto.decryptTarjeta(tarjeta);
			
			SetDebitcard setDebit = new SetDebitcard();
			setDebit.setAmount(cantidad);
			setDebit.setCard(tarjeta);
			setDebit.setClient(propsFile.getPrevivaleWsClient());
			setDebit.setCardExpiration(Integer.parseInt(validez));
			setDebit.setFolio(foliador.generarFolio());
			
			SetDebitcardResponse setDebitResp = null;
			
			try {
				setDebitResp = (SetDebitcardResponse) soapConnect.callWebService(setDebit, PreviValeConstants.SOAP_ACTION_SETDEBITCARD);
			} catch(Exception ex) {
				LOGGER.error("Error al consumir el WS de PreviVale: " + ex.getMessage());
				ex.printStackTrace();
				return new ApiResponse(StatusConstants.ERROR_WS_PREVIVALE, StatusConstants.ERROR_WS_PREVIVALE_MSG, baseResp);
			}
			
			LOGGER.debug("Auth Resp: " + setDebitResp.getSetDebitcardResult().getAuthorization());
			LOGGER.debug("Code Resp: " + setDebitResp.getSetDebitcardResult().getCodeReturn());
			
			baseResp = new BaseResponse();
			baseResp.setAuthorization(setDebitResp.getSetDebitcardResult().getAuthorization());
			baseResp.setPrevivaleCode(setDebitResp.getSetDebitcardResult().getCodeReturn());
			
			if(setDebitResp.getSetDebitcardResult().getCodeReturn() == 0) {
				statusCode = StatusConstants.SUCCESS_OPERATION;
				statusMsg = StatusConstants.DISPERSE_CASH_MSG;
			}
			
			long idPeticion = persistReq.saveRequest(PreviValeConstants.SOAP_ACTION_SETDEBITCARD, setDebit, baseResp.toString(), idUsuario, idEstablecimiento);
			baseResp.setIdPeticion(idPeticion);
		} else {
			LOGGER.error("El usuario esta bloqueado, no se podra retirar dinero de la tarjeta");
			return new ApiResponse(StatusConstants.USER_BLOCKED, StatusConstants.USER_BLOCKED_MSG, baseResp);
		}
		
		return new ApiResponse(statusCode, statusMsg, baseResp);
	}

}
