/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.addcel.previvale.domain.LcpfEstablecimiento;
import com.addcel.previvale.domain.TUsuarios;
import com.addcel.previvale.repositories.LcpfEstableciemientoRepository;
import com.addcel.previvale.repositories.TUsuariosRepository;

@Component
public class ValidarUsuarios {
	
	private static final Logger LOGGER = LogManager.getLogger(ValidarUsuarios.class);

	@Autowired
	private TUsuariosRepository tUsuariosRepo;
	
	@Autowired
	private LcpfEstableciemientoRepository lcpfEstableciemientoRepo;
	
	public Map<String, Object> getUserStatus(Long idUsuario, Long idEstablecimiento) {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer status = 0;
		String fullname = "";
		
		if(idUsuario != null) {
			LOGGER.debug("Buscando en la BD al usuario: " + idUsuario);
			TUsuarios usuario = tUsuariosRepo.findOne(idUsuario);
			
			if(usuario != null) {
				LOGGER.debug("Usuario encontrado: " + usuario.toString());
				status = usuario.getIdUsrStatus();
				fullname = usuario.getUsrNombre() + " " + usuario .getUsrApellido();
			}
			
		} else if(idEstablecimiento != null) {
			LOGGER.debug("Buscando en la BD al negocio: " + idEstablecimiento);
			LcpfEstablecimiento establecimiento = lcpfEstableciemientoRepo.findOne(idEstablecimiento);
			
			if(establecimiento != null) {
				LOGGER.debug("Establecimiento encontrado: " + establecimiento.toString());
				status = establecimiento.getEstatus();
				fullname = establecimiento.getRepresentanteNombre() + " " + establecimiento.getRepresentantePaterno();
			}
			
		} else {
			LOGGER.warn("No se envio ningun id para usuario ni para establecimiento");
		}
		
		map.put("status", status);
		map.put("fullname", fullname);
		
		return map;
	}
	
}
