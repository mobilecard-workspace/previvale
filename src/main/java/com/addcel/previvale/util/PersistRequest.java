/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.util;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.addcel.previvale.domain.WsPreviVale;
import com.addcel.previvale.repositories.WsPrevivaleRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class PersistRequest {
	
	@Autowired
	private WsPrevivaleRepository wsPreviValeRepo;
	
	private static final Logger LOGGER = LogManager.getLogger(PersistRequest.class);
	
	public long saveRequest(String metodo, Object peticion, String respuesta, Long idUsuario, Long idEstablecimiento) {
		ObjectMapper mapper = new ObjectMapper();
		String request = "";
		WsPreviVale wsPreviNew = null;
		
		try {
			request = mapper.writeValueAsString(peticion);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			LOGGER.error("No se pudo parsear a JSON la peticion: " + e.getMessage());
		}
				
		WsPreviVale wsPreviVale = new WsPreviVale();
		wsPreviVale.setMetodo(metodo);
		wsPreviVale.setPeticion(request);
		wsPreviVale.setRespuesta(respuesta);
		wsPreviVale.setIdUsuario(idUsuario);
		wsPreviVale.setIdEstablecimiento(idEstablecimiento);
		wsPreviVale.setFecha(new Date());
		
		try {
			LOGGER.debug("Guardando la peticion en la BD ...");
			wsPreviNew = wsPreviValeRepo.save(wsPreviVale);
		} catch(Exception ex) {
			LOGGER.error("Ocurrio un error al guardar la peticion en la BD: " + ex.getMessage());
			ex.printStackTrace();
		}
				
		if(wsPreviNew != null){
			LOGGER.debug("Peticion guardada en la BD: " + wsPreviNew.toString());
			return wsPreviNew.getIdPeticion();
		} else {
			LOGGER.warn("No se pudo guardar la peticion en la BD");
			return 0;
		}
	}

}
