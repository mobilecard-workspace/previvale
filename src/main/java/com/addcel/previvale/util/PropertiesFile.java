/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@PropertySource("classpath:application.properties")
@Data
public class PropertiesFile {
	
	@Value("${previvale.ws.url}")
	private String previvaleWsUrl;
	
	@Value("${previvale.ws.username}")
	private String previvaleWsUsername;
	
	@Value("${previvale.ws.password}")
	private String previvaleWsPassword;
	
	@Value("${previvale.ws.client}")
	private String previvaleWsClient;
	
	@Value("${push.service.url}")
	private String pushServiceUrl;
	
	@Value("${push.service.username}")
	private String pushServiceUsername;
	
	@Value("${push.service.password}")
	private String pushServicePassword;
	
	@Value("${push.modulo.previvale}")
	private String pushModuloPrevivale;
	
}
