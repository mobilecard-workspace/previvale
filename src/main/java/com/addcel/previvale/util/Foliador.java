/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.util;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class Foliador {
	
	private static final Logger LOGGER = LogManager.getLogger(Foliador.class);

	public String generarFolio() {
		SecureRandom random = new SecureRandom();
		String text = new BigInteger(130, random).toString(32);
		
		LOGGER.debug("Folio generado: " + text);
		return text;
	}

}
