/**
 * @author Victor Ramairez
 */

package com.addcel.previvale.util;

import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class DataValidator {
	
	private static final Logger LOGGER = LogManager.getLogger(DataValidator.class);

	public boolean validateCURP(String curp) {
		boolean valido = false;
		
		LOGGER.debug("Validando la CURP: " + curp);
		
		String regex =
				"[A-Z]{1}[AEIOU]{1}[A-Z]{2}[0-9]{2}" +
				"(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])" +
				"[HM]{1}" +
				"(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)" +
				"[B-DF-HJ-NP-TV-Z]{3}" +
				"[0-9A-Z]{1}[0-9]{1}$";
		
		Pattern patron = Pattern.compile(regex);
		
		if(!patron.matcher(curp).matches()) {
			LOGGER.warn("El formato de la CURP NO es correcto!");
			return false;
		} else { 
			valido = true;
			LOGGER.debug("El formato de la CURP SI es correcto");
		}
		
		return valido;
	}
		
	public boolean validateRFC(String rfc) {
		boolean valido = false;
		
		LOGGER.debug("Validando el RFC: " + rfc);
		
		String regex = "[A-Z,�,&]{4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?$";

		Pattern patron = Pattern.compile(regex);
	    
		if(!patron.matcher(rfc).matches()) {
			LOGGER.warn("El formato del RFC NO es correcto!");
			return false;
		} else { 
			valido = true;
			LOGGER.debug("El formato del RFC SI es correcto");
		}
		
		return valido;
	}
	
	public boolean validateFecha(String fecha) {
		LOGGER.debug("Validando la fecha: " + fecha);
		
		if(fecha.length() != 8) {
			LOGGER.error("La longitud de la fecha es incorrecta!");
			return false;
		}
		
		LOGGER.debug("La fecha SI es valida");
		
		return true;
	}
	
}
