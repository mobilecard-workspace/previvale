/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.util;

import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.addcel.previvale.ws.PushResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class RestClient {
	
	private static final Logger LOGGER = LogManager.getLogger(RestClient.class);
	
	@Autowired
	private PropertiesFile propsFile;
		
	/**
	 * Metodo para consumir una API REST
	 * @param uri url del servicio rest
	 * @return
	 * @throws IOException 
	 * @throws Exception
	 */
	public PushResponse createRequest(Object requestBody, String uri, HttpMethod httpMethod) 
			throws RestClientException, IOException {
		
		RestTemplate restTemplate = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		PushResponse response = null;
		HttpEntity<String> entity = null;
		HttpHeaders headers = null;
		
		LOGGER.debug("{} - {} ", httpMethod.toString(), uri);
		
		headers = addHeadersPush();

		String body = mapper.writeValueAsString(requestBody);
						
		entity = new HttpEntity<String>(body, headers);
		LOGGER.debug("Request Body: " + entity.getBody());
			
		LOGGER.debug("Header: Content-Type - {}", entity.getHeaders().getContentType() != null ?
				entity.getHeaders().getContentType().toString() : "");
		LOGGER.debug("Header: Accept - {}", entity.getHeaders().get("Accept") != null ?
				Arrays.toString(entity.getHeaders().get("Accept").toArray()) : "");
		LOGGER.debug("Header: Authorization - {}", entity.getHeaders().get("Authorization") != null ?
				Arrays.toString(entity.getHeaders().get("Authorization").toArray()) : "");
		
		ResponseEntity<PushResponse> result = restTemplate.exchange(uri, httpMethod, entity, PushResponse.class);
		
		LOGGER.debug("Status Code: " + result.getStatusCode());
		LOGGER.debug("Body Response: " + result.getBody().toString());
		
		response = result.getBody();
		
		return response;
	}
		
	private HttpHeaders addHeadersPush() {
		LOGGER.debug("Agregando headers para Push Notifications ...");
		String basicAuth = propsFile.getPushServiceUsername() + ":" + propsFile.getPushServicePassword();
		basicAuth = Base64.getEncoder().encodeToString(basicAuth.getBytes());
		
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Basic " + basicAuth);
		
		return headers;
	}
	
}
