/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.previvale.constants.StatusConstants;
import com.addcel.previvale.request.TarjetaRequest;
import com.addcel.previvale.response.ApiResponse;
import com.addcel.previvale.services.RegistroClientesService;
import com.fasterxml.uuid.Generators;

@RestController
@RequestMapping("/api")
public class RegistroClientesController {
	
	@Autowired
	private RegistroClientesService registroClientesServ;
	
	private static final Logger LOGGER = LogManager.getLogger(RegistroClientesController.class);
	
	@PostMapping(value = "/{idApp}/{idPais}/{idioma}/registrarCliente", produces = MediaType.APPLICATION_JSON_VALUE)
	public Object registrarCliente(@RequestBody TarjetaRequest tarjetaReq, @PathVariable int idApp,
			@PathVariable int idPais, @PathVariable String idioma) {
		ThreadContext.put("sessionID", String.valueOf(Generators.timeBasedGenerator().generate().timestamp()));
		ApiResponse resp = new ApiResponse();
		
		LOGGER.info("Registrando el cliente: " + tarjetaReq.getIdUsuario());
		
		resp = registroClientesServ.registroCliente(tarjetaReq.getIdUsuario(), idApp);
		
		if(resp.getCode() == StatusConstants.SUCCESS_OPERATION) {
			LOGGER.info("Se ha registrado correctamente el cliente: " + tarjetaReq.getIdUsuario());
		} else {
			LOGGER.warn("No se pudo registrar el cliente: " + tarjetaReq.getIdUsuario());
		}
		
		LOGGER.debug("Respuesta enviada al cliente: " + resp.toString());
		ThreadContext.clearAll();
		
		return resp;
	}
	
	@PostMapping(value = "/{idApp}/{idPais}/{idioma}/registrarClienteConTarjeta", produces = MediaType.APPLICATION_JSON_VALUE)
	public Object registrarClienteConTarjeta(@RequestBody TarjetaRequest tarjetaReq, @PathVariable int idApp,
			@PathVariable int idPais, @PathVariable String idioma) {
		ThreadContext.put("sessionID", String.valueOf(Generators.timeBasedGenerator().generate().timestamp()));
		ApiResponse resp = new ApiResponse();
		
		LOGGER.info("Registrando al cliente: " + tarjetaReq.getIdUsuario() + 
				" con la tarjeta: " + tarjetaReq.getTarjeta());
		
		resp = registroClientesServ.registroClienteConTarjeta(tarjetaReq.getIdUsuario(), tarjetaReq.getTarjeta(), idApp);
		
		if(resp.getCode() == StatusConstants.SUCCESS_OPERATION) {
			LOGGER.info("Se ha registrado correctamente la tarjeta: [" + tarjetaReq.getTarjeta() + "] para el cliente: [" + 
					tarjetaReq.getIdUsuario() + "]");
		} else {
			LOGGER.warn("No se pudo registrar el cleinte: " + tarjetaReq.getIdUsuario() + 
					" con la tarjeta: " + tarjetaReq.getTarjeta());
		}
		
		LOGGER.debug("Respuesta enviada al cliente: " + resp.toString());
		ThreadContext.clearAll();
		
		return resp;
	}

}
