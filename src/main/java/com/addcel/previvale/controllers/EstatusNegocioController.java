/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.previvale.constants.StatusConstants;
import com.addcel.previvale.request.TarjetaRequest;
import com.addcel.previvale.response.ApiResponse;
import com.addcel.previvale.services.EstatusNegocioService;
import com.fasterxml.uuid.Generators;

@RestController
@RequestMapping("/api")
public class EstatusNegocioController {
	
	@Autowired
	private EstatusNegocioService estatusNegocioServ;
	
	private static final Logger LOGGER = LogManager.getLogger(EstatusNegocioController.class);
	
	@PostMapping(value = "/{idApp}/{idPais}/{idioma}/estatusTarjetaNegocio", produces = MediaType.APPLICATION_JSON_VALUE)
	public Object estatusTarjetaNegocio(@RequestBody TarjetaRequest tarjetaReq, @PathVariable int idApp,
			@PathVariable int idPais, @PathVariable String idioma) {
		ThreadContext.put("sessionID", String.valueOf(Generators.timeBasedGenerator().generate().timestamp()));
		ApiResponse resp = new ApiResponse();
		
		LOGGER.info("Buscando el estatus de la tarjeta para el negocio: " + tarjetaReq.getIdEstablecimiento());
		
		resp = estatusNegocioServ.estatusTarjetaNegocio(tarjetaReq.getIdEstablecimiento());
		
		if(resp.getCode() == StatusConstants.SUCCESS_OPERATION) {
			LOGGER.info("Se ha consultado correctamente el estatus de la tarjeta del negocio: " + tarjetaReq.getIdEstablecimiento());
		} else {
			LOGGER.warn("Se pudo consultar el estatus de la tarjeta del negocio: " + tarjetaReq.getIdEstablecimiento());
		}
		
		LOGGER.debug("Respuesta enviada al cliente: " + resp.toString());
		ThreadContext.clearAll();
		
		return resp;
	}

}
