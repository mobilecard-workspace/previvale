/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.previvale.constants.StatusConstants;
import com.addcel.previvale.request.AgregarRequest;
import com.addcel.previvale.request.RetirarRequest;
import com.addcel.previvale.response.ApiResponse;
import com.addcel.previvale.services.EfectivoTarjetaService;
import com.fasterxml.uuid.Generators;

@RestController
@RequestMapping("/api")
public class EfectivoTarjetaController {
	
	@Autowired
	private EfectivoTarjetaService efectivoTarjetaServ;
	
	private static final Logger LOGGER = LogManager.getLogger(EfectivoTarjetaController.class);
	
	@PostMapping(value = "/{idApp}/{idPais}/{idioma}/agregarDinero", produces = MediaType.APPLICATION_JSON_VALUE)
	public Object agregarDinero(@RequestBody AgregarRequest agregarReq, @PathVariable int idApp,
			@PathVariable int idPais, @PathVariable String idioma) {
		ThreadContext.put("sessionID", String.valueOf(Generators.timeBasedGenerator().generate().timestamp()));
		ApiResponse resp = new ApiResponse();
		
		LOGGER.info("Agregando dinero a la tarjeta: " + agregarReq.getTarjeta());
		
		resp = efectivoTarjetaServ.agregaDinero(agregarReq.getIdUsuario(), agregarReq.getIdEstablecimiento(), agregarReq.getTarjeta(), 
				agregarReq.getCantidad(), idApp, idPais, idioma);
		
		if(resp.getCode() == StatusConstants.SUCCESS_OPERATION) {
			LOGGER.info("Se agrego correctamente dinero a la tarjeta: " + agregarReq.getTarjeta());
		} else {
			LOGGER.warn("No se pudo agregar dinero a la tarjeta: " + agregarReq.getTarjeta());
		}
		
		LOGGER.debug("Respuesta enviada al cliente: " + resp.toString());
		ThreadContext.clearAll();
		return resp;
	}
	
	@PostMapping(value = "/{idApp}/{idPais}/{idioma}/retirarDinero", produces = MediaType.APPLICATION_JSON_VALUE)
	public Object retirarDinero(@RequestBody RetirarRequest retirarReq, @PathVariable int idApp,
			@PathVariable int idPais, @PathVariable String idioma) {
		ThreadContext.put("sessionID", String.valueOf(Generators.timeBasedGenerator().generate().timestamp()));
		ApiResponse resp = new ApiResponse();
		
		LOGGER.info("Retirando dinero de la tarjeta: " + retirarReq.getTarjeta());
		
		resp = efectivoTarjetaServ.retiraDinero(retirarReq.getIdUsuario(), retirarReq.getIdEstablecimiento(), retirarReq.getTarjeta(), 
				retirarReq.getCantidad(), retirarReq.getValidez());
		
		if(resp.getCode() == StatusConstants.SUCCESS_OPERATION) {
			LOGGER.info("Se retiro correctamente dinero de la tarjeta: " + retirarReq.getTarjeta());
		} else {
			LOGGER.warn("No se pudo retirar dinero de la tarjeta: " + retirarReq.getTarjeta());
		}
		
		LOGGER.debug("Respuesta enviada al cliente: " + resp.toString());
		ThreadContext.clearAll();
		
		return resp;
	}

}
