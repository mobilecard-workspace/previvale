/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.previvale.constants.StatusConstants;
import com.addcel.previvale.request.MovimientosRequest;
import com.addcel.previvale.request.TarjetaRequest;
import com.addcel.previvale.response.ApiResponse;
import com.addcel.previvale.services.MovimientosTarjetaService;
import com.fasterxml.uuid.Generators;

@RestController
@RequestMapping("/api")
public class MovimientosTarjetaController {
	
	@Autowired
	private MovimientosTarjetaService movimientosTarjetaServ;
	
	private static final Logger LOGGER = LogManager.getLogger(MovimientosTarjetaController.class);
	
	@PostMapping(value = "/{idApp}/{idPais}/{idioma}/movimientosTarjeta", produces = MediaType.APPLICATION_JSON_VALUE)
	public Object movimientosTarjeta(@RequestBody MovimientosRequest movimientosReq, @PathVariable int idApp,
			@PathVariable int idPais, @PathVariable String idioma) {
		ThreadContext.put("sessionID", String.valueOf(Generators.timeBasedGenerator().generate().timestamp()));
		ApiResponse resp = new ApiResponse();
		
		LOGGER.info("Obteniendo los movimientos de la tarjeta: " + movimientosReq.getTarjeta());
		
		resp = movimientosTarjetaServ.movimientosTarjeta(movimientosReq.getIdUsuario(), movimientosReq.getIdEstablecimiento(),
				movimientosReq.getTarjeta(), movimientosReq.getFechaIni(), movimientosReq.getFechaFin());
		
		if(resp.getCode() == StatusConstants.SUCCESS_OPERATION) {
			LOGGER.info("Se obtuvieron correctamente los movimientos de la tarjeta: " + movimientosReq.getTarjeta());
		} else {
			LOGGER.warn("No se pudo obtener los movimientos de la tarjeta: " + movimientosReq.getTarjeta());
		}
		
		LOGGER.debug("Respuesta enviada al cliente: " + resp.toString());
		ThreadContext.clearAll();
		
		return resp;
	}
	
	@PostMapping(value = "/{idApp}/{idPais}/{idioma}/creditoTarjeta", produces = MediaType.APPLICATION_JSON_VALUE)
	public Object creditoTarjeta(@RequestBody TarjetaRequest tarjetaReq, @PathVariable int idApp,
			@PathVariable int idPais, @PathVariable String idioma) {
		ThreadContext.put("sessionID", String.valueOf(Generators.timeBasedGenerator().generate().timestamp()));
		ApiResponse resp = new ApiResponse();
		
		LOGGER.info("Obteniendo el credito de la tarjeta: ");
		
		resp = movimientosTarjetaServ.creditoTarjeta(tarjetaReq.getIdUsuario(), tarjetaReq.getIdEstablecimiento(), tarjetaReq.getTarjeta());
		
		if(resp.getCode() == StatusConstants.SUCCESS_OPERATION) {
			LOGGER.info("Se obtuvo correctamente el credito de la tarjeta: ");
		} else {
			LOGGER.warn("No se pudo obtener el credito de la tarjeta: ");
		}
		
		LOGGER.debug("Respuesta enviada al cliente: " + resp.toString());
		ThreadContext.clearAll();
		
		return resp;
	}

}
