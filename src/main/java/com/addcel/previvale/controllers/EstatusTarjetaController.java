/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.previvale.constants.StatusConstants;
import com.addcel.previvale.request.TarjetaRequest;
import com.addcel.previvale.response.ApiResponse;
import com.addcel.previvale.services.EstatusTarjetaService;
import com.fasterxml.uuid.Generators;

@RestController
@RequestMapping("/api")
public class EstatusTarjetaController {
	
	@Autowired
	private EstatusTarjetaService estatusTarjetaServ;
	
	private static final Logger LOGGER = LogManager.getLogger(EstatusTarjetaController.class);
	
	@PostMapping(value = "/{idApp}/{idPais}/{idioma}/bloquearTarjeta", produces = MediaType.APPLICATION_JSON_VALUE)
	public Object bloquearTarjeta(@RequestBody TarjetaRequest tarjetaReq, @PathVariable int idApp,
			@PathVariable int idPais, @PathVariable String idioma) {
		ThreadContext.put("sessionID", String.valueOf(Generators.timeBasedGenerator().generate().timestamp()));
		ApiResponse resp = new ApiResponse();
		
		LOGGER.info("Bloqueando tarjeta: " + tarjetaReq.getTarjeta());
		
		resp = estatusTarjetaServ.bloqueoTarjeta(tarjetaReq.getIdUsuario(), tarjetaReq.getIdEstablecimiento(), tarjetaReq.getTarjeta());
		
		if(resp.getCode() == StatusConstants.SUCCESS_OPERATION) {
			LOGGER.info("Se ha bloqueado correctamente la tarjeta: " + tarjetaReq.getTarjeta());
		} else {
			LOGGER.warn("No se pudo bloquear la tarjeta: " + tarjetaReq.getTarjeta());
		}
		
		LOGGER.debug("Respuesta enviada al cliente: " + resp.toString());
		ThreadContext.clearAll();
		
		return resp;
	}
	
	@PostMapping(value = "/{idApp}/{idPais}/{idioma}/activarTarjeta", produces = MediaType.APPLICATION_JSON_VALUE)
	public Object activarTarjeta(@RequestBody TarjetaRequest tarjetaReq, @PathVariable int idApp,
			@PathVariable int idPais, @PathVariable String idioma) {
		ThreadContext.put("sessionID", String.valueOf(Generators.timeBasedGenerator().generate().timestamp()));
		ApiResponse resp = new ApiResponse();
		
		LOGGER.info("Activando tarjeta: " + tarjetaReq.getTarjeta());
		
		resp = estatusTarjetaServ.activacionTarjeta(tarjetaReq.getIdUsuario(), tarjetaReq.getIdEstablecimiento(), tarjetaReq.getTarjeta());
		
		if(resp.getCode() == StatusConstants.SUCCESS_OPERATION) {
			LOGGER.info("Se ha activado correctamente la tarjeta: " + tarjetaReq.getTarjeta());
		} else {
			LOGGER.warn("No se pudo activar la tarjeta: " + tarjetaReq.getTarjeta());
		}
		
		LOGGER.debug("Respuesta enviada al cliente: " + resp.toString());
		ThreadContext.clearAll();
		
		return resp;
	}
	
	@PostMapping(value = "/{idApp}/{idPais}/{idioma}/estatusTarjeta", produces = MediaType.APPLICATION_JSON_VALUE)
	public Object estatusTarjeta(@RequestBody TarjetaRequest tarjetaReq, @PathVariable int idApp,
			@PathVariable int idPais, @PathVariable String idioma) {
		ThreadContext.put("sessionID", String.valueOf(Generators.timeBasedGenerator().generate().timestamp()));
		ApiResponse resp = new ApiResponse();
		
		LOGGER.info("Obteniendo estatus de la tarjeta: " + tarjetaReq.getTarjeta());
		
		resp = estatusTarjetaServ.estatusTarjeta(tarjetaReq.getIdUsuario(), tarjetaReq.getIdEstablecimiento(), tarjetaReq.getTarjeta());
		
		if(resp.getCode() == StatusConstants.SUCCESS_OPERATION) {
			LOGGER.info("Se obtuvo el estatus correctamente la tarjeta: " + tarjetaReq.getTarjeta());
		} else {
			LOGGER.warn("No se pudo obtener el status de la tarjeta: " + tarjetaReq.getTarjeta());
		}
		
		LOGGER.debug("Respuesta enviada al cliente: " + resp.toString());
		ThreadContext.clearAll();
		
		return resp;
	}
	
}
