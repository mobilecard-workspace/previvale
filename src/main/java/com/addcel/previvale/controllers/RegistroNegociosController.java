/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.previvale.constants.StatusConstants;
import com.addcel.previvale.request.TarjetaRequest;
import com.addcel.previvale.response.ApiResponse;
import com.addcel.previvale.services.RegistroNegociosService;
import com.fasterxml.uuid.Generators;

@RestController
@RequestMapping("/api")
public class RegistroNegociosController {
	
	@Autowired
	private RegistroNegociosService registroNegociosServ;
	
	private static final Logger LOGGER = LogManager.getLogger(RegistroNegociosController.class);
	
	@PostMapping(value = "/{idApp}/{idPais}/{idioma}/registrarNegocioSinTarjeta", produces = MediaType.APPLICATION_JSON_VALUE)
	public Object registrarNegocioSinTarjeta(@RequestBody TarjetaRequest tarjetaReq, @PathVariable int idApp,
			@PathVariable int idPais, @PathVariable String idioma) {
		ThreadContext.put("sessionID", String.valueOf(Generators.timeBasedGenerator().generate().timestamp()));
		ApiResponse resp = new ApiResponse();
		
		LOGGER.info("Registrando el negocio: " + tarjetaReq.getIdEstablecimiento());
		
		resp = registroNegociosServ.registroSinTarjeta(tarjetaReq.getIdEstablecimiento(), idApp);
		
		if(resp.getCode() == StatusConstants.SUCCESS_OPERATION) {
			LOGGER.info("Se ha registrado correctamente el negocio: " + tarjetaReq.getIdEstablecimiento());
		} else {
			LOGGER.warn("No se pudo registrar el negocio: " + tarjetaReq.getIdEstablecimiento());
		}
		
		LOGGER.debug("Respuesta enviada al cliente: " + resp.toString());
		ThreadContext.clearAll();
		
		return resp;
	}
	
	@PostMapping(value = "/{idApp}/{idPais}/{idioma}/registrarNegocioConTarjeta", produces = MediaType.APPLICATION_JSON_VALUE)
	public Object registrarNegocioConTarjeta(@RequestBody TarjetaRequest tarjetaReq, @PathVariable int idApp,
			@PathVariable int idPais, @PathVariable String idioma) {
		ThreadContext.put("sessionID", String.valueOf(Generators.timeBasedGenerator().generate().timestamp()));
		ApiResponse resp = new ApiResponse();
		
		LOGGER.info("Registrando el negocio: " + tarjetaReq.getIdEstablecimiento() + 
				" con la tarjeta: " + tarjetaReq.getTarjeta());
		
		resp = registroNegociosServ.registroConTarjeta(tarjetaReq.getIdEstablecimiento(), tarjetaReq.getTarjeta(), idApp);
		
		if(resp.getCode() == StatusConstants.SUCCESS_OPERATION) {
			LOGGER.info("Se ha registrado correctamente la tarjeta: [" + tarjetaReq.getTarjeta() + "] para el negocio: [" + 
					tarjetaReq.getIdEstablecimiento() + "]");
		} else {
			LOGGER.warn("No se pudo registrar el negocio: " + tarjetaReq.getIdEstablecimiento() + 
					" con la tarjeta: " + tarjetaReq.getTarjeta());
		}
		
		LOGGER.debug("Respuesta enviada al cliente: " + resp.toString());
		ThreadContext.clearAll();
		return resp;
	}

}
