/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TarjetaRequest {

	private Long idUsuario;
	private Long idEstablecimiento;
	private String tarjeta;
	
}
