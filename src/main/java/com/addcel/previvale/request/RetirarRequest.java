/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RetirarRequest extends AgregarRequest {

	private String validez;
	
}
