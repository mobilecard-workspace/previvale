/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.constants;

public class StatusConstants {

	public static final int SUCCESS_OPERATION = 1000;
	public static final int ERROR_WS_PREVIVALE = 2100;
	public static final int BAD_RFC = 2200;
	public static final int BAD_CURP = 2300;
	public static final int USER_BLOCKED = 2400;
	public static final int BAD_DATE = 2500;
	public static final int UNSUCCESS_WS_PREVIVALE = 2600;
	public static final int CARD_STOCK_NOT_AVAILABLE = 2700;
	public static final int WALLET_NOTFOUND = 2800;
	public static final int FAIL_OPERATION = 2900;
	public static final int NEGOCIO_NOTFOUND = 3000;
	public static final int CLABE_NOTFOUND = 3100;
	public static final int MOVEMENTS_ERROR = 3200;
	public static final int MOVEMENTS_EMPTY = 3300;
		
	public static final String BLOCK_CARD_MSG = "La tarjeta se ha bloqueado correctamente";
	public static final String ACTIVATE_CARD_MSG = "La tarjeta se ha activado correctamente";
	public static final String REGISTER_CLIENT = "Se ha registrado correctamente al cliente";
	public static final String STATUS_CARD_MSG = "Se ha obtenido correctamente el estatus de la tarjeta";
	public static final String STATUS_CARD_NEGOCIO_MSG = "Se ha obtenido correctamente el estatus de la tarjeta para el negocio";
	public static final String ADD_CASH_MSG = "Se ha agregado correctamente el dinero a la tarjeta";
	public static final String DISPERSE_CASH_MSG = "Se ha retirado correctamente el dinero de la tarjeta";
	public static final String MOVS_CARD_MSG = "Se han obtenido correctamente los movimientos de la tarjeta";
	public static final String CREDIT_CARD_MSG = "Se ha obtenido correctamente el credito disponible de la tarjeta";
	public static final String REGISTER_CARD_MSG = "Se ha registrado correctamente el usuario y la tarjeta";
	
	public static final String FAIL_OPERATION_MSG = "Su solicitud no pudo ser procesada, int�ntelo nuevamente. C�digo de error: PV" + FAIL_OPERATION;
	public static final String BAD_RFC_MSG = "Su solicitud no pudo ser procesada, int�ntelo nuevamente. C�digo de error: PV" + BAD_RFC;
	public static final String BAD_CURP_MSG = "Su solicitud no pudo ser procesada, int�ntelo nuevamente. C�digo de error: PV" + BAD_CURP;
	public static final String USER_BLOCKED_MSG = "Su solicitud no pudo ser procesada, int�ntelo nuevamente. C�digo de error: PV" + USER_BLOCKED;
	public static final String BAD_DATE_MSG = "Su solicitud no pudo ser procesada, int�ntelo nuevamente. C�digo de error: PV" + BAD_DATE;
	public static final String ERROR_WS_PREVIVALE_MSG = "Su solicitud no pudo ser procesada, int�ntelo nuevamente. C�digo de error: PV" + ERROR_WS_PREVIVALE;
	public static final String UNSUCCESS_WS_PREVIVALE_MSG = "Su solicitud no pudo ser procesada, int�ntelo nuevamente. C�digo de error: PV" + UNSUCCESS_WS_PREVIVALE;
	public static final String CARD_STOCK_NOT_AVAILABLE_MSG = "Su solicitud no pudo ser procesada, int�ntelo nuevamente. C�digo de error: PV" + CARD_STOCK_NOT_AVAILABLE;
	public static final String NEGOCIO_NOTFOUND_MSG = "Su solicitud no pudo ser procesada, int�ntelo nuevamente. C�digo de error: PV" + NEGOCIO_NOTFOUND;
	public static final String WALLET_NOTFOUND_MSG = "Su solicitud no pudo ser procesada, int�ntelo nuevamente. C�digo de error: PV" + WALLET_NOTFOUND;
	public static final String CLABE_NOTFOUND_MSG = "Su solicitud no pudo ser procesada, int�ntelo nuevamente. C�digo de error: PV" + CLABE_NOTFOUND;
	public static final String MOVEMENTS_ERROR_MSG = "Su solicitud no pudo ser procesada, int�ntelo nuevamente. C�digo de error: PV" + MOVEMENTS_ERROR;
	public static final String MOVEMENTS_EMPTY_MSG = "Su solicitud no pudo ser procesada, int�ntelo nuevamente. C�digo de error: PV" + MOVEMENTS_EMPTY;
}
