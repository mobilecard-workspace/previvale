/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.constants;

public class PushConstants {
	
	public static final String PUSH_TIPO_USUARIO = "USUARIO";
	public static final String PUSH_TIPO_NEGOCIO = "NEGOCIO";
	
	public static final int PUSH_SUCCESS = 0;
	
	public static final String PARAM_NOMBRES = "<nombres>";
	public static final String PARAM_MONTO = "<monto>";
	public static final String PARAM_TARJETA = "<tarjeta>";
	
}
