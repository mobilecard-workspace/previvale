/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.constants;

public class MobileCardConstants {
	
	public static final int USUARIO_ACTIVO = 1;
	public static final int USUARIO_ACTIVO_2 = 99;
	public static final int USUARIO_BLOQUEADO = 3;
	public static final int USUARIO_RESET = 98;
	
	public static final char PREVIVALE_CARD_STATUS_ACTIVE = 'Y';
	public static final char PREVIVALE_CARD_STATUS_INACTIVE = 'N';
	
	public static final String IS_PREVIVALE_TRUE = "1";
	
	public static final int WALLET_ID_FRANQUICIA = 11;
	public static final int WALLET_ID_TARJETASTIPO = 1;
	
	public static final int CLABE_ACTIVA = 1;
	public static final int CLABE_INACTIVA = 0;
	
}
