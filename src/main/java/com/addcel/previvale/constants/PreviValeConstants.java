/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.constants;

public class PreviValeConstants {
	
	public static final String SOAP_ACTION_GETSTATUS = "GetStatus";
	public static final String SOAP_ACTION_SETLOCK = "Setlock";
	public static final String SOAP_ACTION_SETUNLOCK = "SetUnlock";
	public static final String SOAP_ACTION_SETDEBITCARD = "SetDebitcard";
	public static final String SOAP_ACTION_ONLINEPAYMENT = "OnlinePayment";
	public static final String SOAP_ACTION_GETMOVEMENTS = "GetMovementsMonth";
	public static final String SOAP_ACTION_GETCREDITLIMIT = "GetCreditLimitAndAvailableData";
	public static final String SOAP_ACTION_USERREGISTER = "UserRegister";
	
}
