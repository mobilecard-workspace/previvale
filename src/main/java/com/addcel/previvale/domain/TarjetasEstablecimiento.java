/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tarjetas_establecimiento")
public class TarjetasEstablecimiento {
	
	@Id
	@Column(name = "id_tarjetas_establecimiento", nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idTarjetasEstablecimiento;
	
	@Column(name = "id_aplicacion", nullable = true)
	private Integer idAplicacion;
	
	@Column(name = "id_establecimiento", nullable = false)
	private Long idEstablecimiento;
	
	@Column(name = "numero_tarjeta", nullable = false)
	private String numeroTarjeta;
	
	@Column(name = "vigencia", nullable = false)
	private String vigencia;
	
	@Column(name = "ct", nullable = false)
	private String ct;
	
	@Column(name = "estatus", nullable = false)
	private Integer estatus;
	
	@Column(name = "idfranquicia", nullable = false)
	private Integer idFranquicia;
	
	@Column(name = "idtarjetas_tipo", nullable = false)
	private Integer idTarjetasTipo;
	
	@Column(name = "fecha_registro", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRegistro;
	
	@Column(name = "nombre_tarjeta", nullable = true)
	private String nombreTarjeta;
	
	@Column(name = "previvale", nullable = true)
	private String previvale;
	
}
