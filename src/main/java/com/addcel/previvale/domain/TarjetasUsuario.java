/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tarjetas_usuario")
public class TarjetasUsuario {
	
	@Id
	@Column(name = "idtarjetasusuario", nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idTarjetasUsuario;
	
	@Column(name = "id_aplicacion", nullable = true)
	private Integer idAplicacion;
	
	@Column(name = "idusuario", nullable = false)
	private Long idUsuario;
	
	@Column(name = "numerotarjeta", nullable = false)
	private String numerotarjeta;
	
	@Column(name = "vigencia", nullable = false)
	private String vigencia;
	
	@Column(name = "ct", nullable = true)
	private String ct;
		
	@Column(name = "idfranquicia", nullable = false)
	private Integer idFranquicia;
	
	@Column(name = "idtarjetas_tipo", nullable = false)
	private Integer idTarjetasTipo;
	
	@Column(name = "fecharegistro", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRegistro;
	
	@Column(name = "nombre_tarjeta", nullable = true)
	private String nombreTarjeta;
	
	@Column(name = "previvale", nullable = true)
	private String previvale;

}
