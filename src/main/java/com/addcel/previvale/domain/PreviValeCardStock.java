/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "previvale_card_stock")
public class PreviValeCardStock {

	@Id
	@Column(name = "id_previvale_card_stock", nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long idPrevivaleCardStock;
	
	@Column(name = "numero", nullable = false)
	private String numero;
	
	@Column(name = "ultimos_numeros", nullable = true)
	private String ultimosNumeros;
	
	@Column(name = "vigencia", nullable = true)
	private String vigencia;
	
	@Column(name = "ct", nullable = true)
	private String ct;
	
	@Column(name = "fecha_activacion", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaActivacion;
	
	@Column(name = "activa", nullable = false)
	private char activa;
	
	@Column(name = "id_usuario", nullable = true)
	private Long idUsuario;
	
	@Column(name = "idtarjetasusuario", nullable = true)
	private Integer idTarjetasUsuario;
	
	@Column(name = "id_establecimiento", nullable = true)
	private Long idEstablecimiento;
	
	@Column(name = "idtarjetas_establecimiento", nullable = true)
	private Long idTarjetasEstablecimiento;
	
	@Column(name = "id_stp_cuentas_clabe", nullable = true, unique = true)
	private Long idStpCuentasClabe;
	
}
