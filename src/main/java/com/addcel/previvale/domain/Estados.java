/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "estados")
public class Estados {

	@Id
	@Column(name = "idedo", nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long idedo;
	
	@Column(name = "nombre", nullable = true)
	private String nombre;
	
	@Column(name = "pais", nullable = true)
	private String pais;
	
	@Column(name = "abreviatura", nullable = true)
	private String abreviatura;
	
}
