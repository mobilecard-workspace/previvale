/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.ws;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PushResponse {

	private int code;
	
	private String message;
	
	@JsonInclude(value = Include.NON_EMPTY)
	private String idPush;
	
	@JsonInclude(value = Include.NON_EMPTY)
	private Long succesDate; 
	
}
