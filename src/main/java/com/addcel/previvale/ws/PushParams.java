/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.ws;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PushParams {

	private String name;
	private String value;
	
}
