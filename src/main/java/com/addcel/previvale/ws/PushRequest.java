/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.ws;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PushRequest {

	private String modulo;
	private long id_usuario;
	private String tipoUsuario;
	private String idioma;
	private int idPais;
	private int idApp;
	private PushParams[] params;
	
}
