/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.addcel.previvale.domain.StpCuentasClabe;

public interface StpCuentasClabeRepository extends CrudRepository<StpCuentasClabe, Long> {

	@Query(value = "select * from stp_cuentas_clabe where activa = :activa and id_stp_cuentas_clabe not in " + 
			"(select p.id_stp_cuentas_clabe from previvale_card_stock p where p.id_stp_cuentas_clabe is not null) " + 
			"order by id_stp_cuentas_clabe asc limit 1", nativeQuery = true)
	public StpCuentasClabe findClabeActiva(@Param("activa") int activa);
	
}