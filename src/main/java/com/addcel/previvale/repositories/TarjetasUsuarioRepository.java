/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.repositories;

import org.springframework.data.repository.CrudRepository;

import com.addcel.previvale.domain.TarjetasUsuario;

public interface TarjetasUsuarioRepository extends CrudRepository<TarjetasUsuario, Long> {
	
	@SuppressWarnings("unchecked")
	public TarjetasUsuario save(TarjetasUsuario tarjetasUsuario);
	
}
