/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.addcel.previvale.domain.PreviValeCardStock;

public interface PreviValeCardStockRepository extends CrudRepository<PreviValeCardStock, Long> {
	
	public List<PreviValeCardStock> findFirst1ByActivaAndIdUsuarioAndIdEstablecimiento(@Param("activa") char activa,
			@Param("idUsuario") Long idUsuario, @Param("idEstableciemiento") Long idEstableciemiento);

	@SuppressWarnings("unchecked")
	public PreviValeCardStock save(PreviValeCardStock previValeCardStock);
	
	public PreviValeCardStock findByNumeroAndActivaAndIdEstablecimientoAndIdUsuario(@Param("numero") String numero, 
			@Param("activa") char activa, @Param("idEstablecimiento") Long idEstablecimiento, @Param("idUsuario") Long idUsuario);
	
}
