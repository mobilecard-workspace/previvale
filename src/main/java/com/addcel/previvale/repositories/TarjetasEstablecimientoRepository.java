/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.addcel.previvale.domain.TarjetasEstablecimiento;

public interface TarjetasEstablecimientoRepository extends CrudRepository<TarjetasEstablecimiento, Long> {
	
	public TarjetasEstablecimiento findByIdEstablecimiento(@Param("idEstableciemiento") Long idEstableciemiento);
	
	@SuppressWarnings("unchecked")
	public TarjetasEstablecimiento save(TarjetasEstablecimiento tarjetasEstablecimiento);

}
