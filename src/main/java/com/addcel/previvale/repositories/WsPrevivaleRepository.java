/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.repositories;

import org.springframework.data.repository.CrudRepository;

import com.addcel.previvale.domain.WsPreviVale;

public interface WsPrevivaleRepository extends CrudRepository<WsPreviVale, Long> {

	@SuppressWarnings("unchecked")
	public WsPreviVale save(WsPreviVale wsPreviVale);
	
}
