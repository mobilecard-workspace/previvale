/**
 * @author Victor Ramirez
 */

package com.addcel.previvale.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.addcel.previvale.domain.Estados;

public interface EstadosRepository extends CrudRepository<Estados, Long> {

	public Estados findByIdedo(@Param("idedo") long idEdo);
	
	@Query(value = "select e.* from estados e where e.idedo < 33 order by e.nombre asc", nativeQuery = true)
	public List<Estados> findMexicoEstados();
	
}
